#!/bin/bash

# invoked by deploy.sh after deploy_backend.sh completes
# all docker images should be stopped and deleted except for the backend
# as such, we just need to build the frontend image and run it

docker-compose up -d --build
# docker build -t frontend . && docker run -d -p 80:80 -p 443:443 frontend:latest