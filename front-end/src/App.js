import React from "react";
import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar"
import Home from "./components/Home"
import Artwork from "./components/Artwork"
import Artists from "./components/Artists"
import Museums from "./components/Museums"
import About from "./components/About"
import Search from "./components/Search"
import Artpage from "./components/Artpage"
import Artistpage from "./components/Artistpage"
import Museumpage from "./components/Museumpage"
import Error from "./components/Error"
import VisualizationRoute from "./components/Visualizations/VisualizationRoute";
import ProviderVisualizationRoute from "./components/Visualizations/ProviderVisualizationRoute";
function App() {
  return (
    <Router>
		<Navbar/>
		<br/>
		<Routes>
			<Route path='/' element={<Home/>} />
			<Route path='/artwork' element={<Artwork/>} />
			<Route path='/artists' element={<Artists/>} />
			<Route path='/museums' element={<Museums/>} />
			<Route path='/about' element={<About/>} />
			<Route path='/search' element={<Search/>} />
			<Route path='/visualizations' element={<VisualizationRoute/>} />
			<Route path='/providervisualizations' element={<ProviderVisualizationRoute/>} />
			<Route path='/artworks/:id' element={<Artpage/>} />
			<Route path='/artists/:id' element={<Artistpage/>} />
			<Route path='/museums/:id' element={<Museumpage/>} />
			<Route path='*' element={<Error/>} />
		</Routes>
    </Router>
  );
}

export default App;
