import React, {useEffect, useState, useRef} from "react";
import Getdata from "./Getdata"
import 'bootstrap/dist/css/bootstrap.min.css';
import Artcards from "./Artcards"
import Artistcards from "./Artistcards"
import Museumcards from "./Museumcards"
import styled from 'styled-components';
import { Button } from '@mui/material';
import SearchBar from "./reuseable/SearchBar";

const MainWrapper = styled.div``;

const SearchWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`

function Empty() {
	return (
		<div>
			<p className="text-center">No matching results 😟</p>
		</div>
	)
}

const Search = (props) => {
	const {searchValSent} = props;

	const [data, setData] = useState({entries:[], total: 0});
	const [searchVal, setSearchVal] = useState("");
	const [instancesPerPage, setInstancesPerPage] = useState(25);
	const [pageNum, setPageNum] = useState(0);
	const [totalInstances, setTotalIntances] = useState(0);
  
	const searchBar = useRef(null);

	useEffect(() => {
		console.log("search val sent" + searchValSent)
		if(searchValSent){
		setSearchVal(searchValSent);
		}
	}, [searchValSent]);

	useEffect(() => {
		updateTableData(pageNum, instancesPerPage);
	  }, [pageNum, instancesPerPage, searchVal]);	

	const handleSearch = (searchValue) => {
		console.log("searching for " + searchValue);
		setSearchVal(searchValue);
		setPageNum(0);
	};
	
	const updateTableData = (page, pageSize) => {
		if (searchVal === "") return;
	
		const newPageNum = page + 1;
		console.log(newPageNum);
		const query = `https://api.gallerydb.me/search?general_string=${searchVal}&page=${newPageNum}&maxResults=${pageSize}`;
		fetch(query)
		  .then((response) => {
			if (response.ok) return response.json();
			throw response;
		  })
		  .then((res) => {
			console.log(res);
			setTotalIntances(res.total);
			const artists = [...res.artists].map(artist => {artist.type="artists"; artist.key=artist.id; return artist});
			const artworks = [...res.artworks].map(artwork => {artwork.type="artworks"; artwork.key=artwork.id; return artwork});
			const museums = [...res.museums].map(museum => {museum.type="museums"; museum.key=museum.id; return museum});
			res.entries = [...artworks, ...museums, ...artists];
			setData(res);
		  })
		  .catch((err) => console.error('ERROR FETCH: ', err));
	};
		const highlightText = (text) => {
			try {
			// Split text on highlight term, include term itself into parts, ignore case
			const parts = text.split(new RegExp(`(${searchVal})`, 'gi'));
			return <span>{parts.map(part => part.toLowerCase() === searchVal.toLowerCase() ? <mark>{part}</mark> : part)}</span>;
			} catch (error) {
			console.log(error);
			}
		};
	
	var artworks = data?.artworks;
	var artists = data?.artists;
	var museums = data?.museums;

	var artworkComp = artworks && artworks?.length != 0 ? Artcards    : Empty;
	var artistComp  = artists  && artists?.length  != 0 ? Artistcards : Empty;
	var museumComp  = museums  && museums?.length  != 0 ? Museumcards : Empty;


	return (
		<MainWrapper>

      		<SearchWrapper>
        		<SearchBar defaultValue={searchValSent} onSearch={handleSearch}/>
      		</SearchWrapper>

		 	<br/>
		 	<h2 className="text-center">Artworks</h2>
			 {

				artworkComp(artworks, searchVal)
			}
			<br/>
			<h2 className="text-center">Artists</h2>
			{
				artistComp(artists, searchVal)
			}
			<br/>
			<h2 className="text-center">Museums</h2>
			{
				museumComp(museums, searchVal)
			}
		</MainWrapper>
	)
};

export default Search;
