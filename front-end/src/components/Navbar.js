import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

import navIcon from './imgs/navbar-icon.jpg';

export default function Navbar() {
	return (

	<div class="row">
		<div className="col-12">
			<nav className="navbar navbar-expand-sm  navbar-light" style={{ backgroundColor: "#d5b895" }}>
				<ul className="navbar-nav d-flex align-items-center bd-highlight example-parent">
				<li className="nav-item">
					<Link className="nav-link" to='/'><img alt="nav-icon" className="img-fluid" height="200" src={navIcon} width="200"/></Link> 
				</li>
				<li className="nav-item">
					<Link className="nav-link" to='/'>Home</Link> 
				</li>
				<li className="nav-item">
					<Link className="nav-link" to='/artwork'>Artwork</Link> 
				</li>
				<li className="nav-item">
					<Link className="nav-link" to='/artists'>Artists</Link> 
				</li>
				<li className="nav-item">
					<Link className="nav-link" to='/museums'>Museums</Link> 
				</li>
				<li className="nav-item">
					<Link className="nav-link" to='/visualizations'>Visualizations</Link> 
				</li>
				<li className="nav-item">
					<Link className="nav-link" to='/providervisualizations'>Provider Visualizations</Link> 
				</li>
				<li className="nav-item">
					<Link className="nav-link" to='/about'>About</Link> 
				</li>
				<li className="nav-item">
					<Link className="nav-link" to='/search'>Search 🔍</Link> 
				</li>
				</ul>
			</nav>
		</div>
	</div>

	);
}
