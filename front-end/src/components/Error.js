import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from "./Navbar";

export default function Error() {
	  return (
		<div>
			<center>
			<h3>404: Page not found!</h3>
			<p>
				Oh no! It looks like this page doesn't exist.
				Click something in the navbar above to head back!
			</p>
			</center>
		</div>
	  );
}
