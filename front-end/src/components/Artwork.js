import React, {useEffect, useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Getdata from "./Getdata"
import Artcards from "./Artcards"
import styled from 'styled-components';
import SearchBar from './reuseable/SearchBar';
import FilterSelect from './reuseable/FilterSelect';
import { DataGrid } from '@mui/x-data-grid';
import { useNavigate } from 'react-router';
import Empty from "./Empty"
import highlightText from "./highlightText"

const MainWrapper = styled.div``;

const SearchWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

var pages = []
var numPages = 20;
var mypg = 1;

export default function Artwork () {
	const [data, setData] = useState({ entries: [], total: 0 });
	const [pageNum, setPageNum] = useState(1);
	const [searchVal, setSearchVal] = useState('');
	const [instancesPerPage, setInstancesPerPage] = useState(24);
	const [selectedFilterState, setSelectedFilterState] = useState({});
	const [filterParamString, setFilterParamString] = useState('');
	const [totalInstances, setTotalInstances] = useState(0);
	const [sortParamString, setSortParamString] = useState('');
	const [info, setInfo] = useState();
	const [showCard, setCard] = useState(true);
	const [q, setQuery] = useState("")
	const navigate = useNavigate();


	const artworkFilterTitles = [
		'Name',
		'Era',
		'Media',
		'Country',
		'Genre'
	  ];
	
	const artworkFilters = [
		['A-E', 'F-L', 'M-P', 'Q-Z'],
		['1300s', '1400s', '1500s', '1600s', '1700s', '1800s', '1900s'],
		['Oil', 'Canvas', 'Panel', 'Unknown', 'Tempera', 'Fresco', 'Wood', 'Paper', 'Watercolor', 'Other'],
		["US", "Italy", "France", "UK", "Germany", "Netherlands", "Switzerland", "Russia", "Spain", "Other"],
		["Allegorical Painting", "Portrait", "Genre Painting", "Landscape", "Cityscape", "Abstract", "Religious Painting", "Mythological Painting"]
	];
	
	  useEffect(() => {
		updateTableData(pageNum, instancesPerPage);
	  }, [
		pageNum,
		instancesPerPage,
		searchVal,
		selectedFilterState,
		filterParamString,
		sortParamString,
	  ]);
	
	  const filterMapping = {
		'era': 'date_created'
	  };
	
	  const mapFilterValues = (filter) => {
		if (filter in filterMapping) {
		  return filterMapping[filter];
		}
		return filter;
	  };
	
	  const handleSearch = (searchValue) => {
		console.log('searching for ' + searchValue);
		setSearchVal(searchValue);
		setPageNum(0);
	  };
	
	  const onCheckboxClick = (title, value) => {
		let newState = selectedFilterState;
	
		if (title in newState) {
		  if (newState[title].includes(value)) {
			newState[title] = newState[title].filter((item) => item !== value);
			if (newState[title].length === 0) {
			  delete newState[title];
			}
		  } else {
			newState[title] = [...newState[title], value];
		  }
		} else {
		  newState[title] = [value];
		}
		let filterParams = `&filter=`;
		for (let key in newState) {
		  const newKey = String(key).replaceAll(' ', '_');
		  filterParams += `${mapFilterValues(
			String(newKey).toLowerCase()
		  )}=${newState[key]
			.map((e) => mapFilterValues(e))
			.join(',')
			.toLowerCase()};`;
		}
		// get rid of last ;
		filterParams = filterParams.slice(0, -1);
	
		if (filterParams !== '&filter=') {
		  setFilterParamString(filterParams.replace("+", "%2B"));
		} else {
		  setFilterParamString('');
		}
	
		setSelectedFilterState(newState);
	  };

	  const columns = [
		{ field: 'name', headerName: 'Title', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1},
		{ field: 'country', headerName: 'Country', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		),	flex: 1 },
		{ field: 'date_created', headerName: 'Year', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
		{ field: 'media', headerName: 'Medium', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
		{ field: 'genre', headerName: 'Genre', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
	  ];  



	
	  const updateTableData = (page, pageSize) => {
		console.log('Updating table');
		const newPageNum = page + 1;
		let query = `https://api.gallerydb.me/search?model=artwork${
		  searchVal !== '' ? `&general_string=${searchVal}` : ''
		}`
		setQuery(query)
		query += `&page=${newPageNum}&maxResults=${pageSize}`;
	
		query += filterParamString;
		query += sortParamString;
		console.log(String(query));
	
		// remove &filter if there are none selected
		const endVal = query.length;
		if (query.slice(endVal - 7, endVal) === '&filter') {
		  query = query.slice(0, endVal - 7);
		}
	
		console.log(query);
		fetch(query)
		  .then((response) => {
			if (response.ok) return response.json();
			throw response;
		  })
		  .then((res) => {
			console.log(res);
			setTotalInstances(res.total);
			res.entries = res['query results'];
			setData(res);
		  })
		  .catch((err) => console.error('ERROR FETCH: ', err));
	  };
	
	  const sortByColumn = (e) => {
		if (e.length <= 0) return;
	
		let sortParam = `&sort=${e[0].field}&sortReverse=${
		  e[0].sort === 'asc' ? 'false' : 'true'
		}`;
		setSortParamString(sortParam);
	  };	

	   // link text that matches page
	   const linkText = (params) => {
		try {
			return <a href={"/artworks/" + params.id}>{String(params.value)}</a>
		} catch (error) {
		  console.log(error);
		}
	  };

	  function Artgrid (currentPosts) {
		const options = [
		  'one', 'two', 'three'
		];
		const defaultOption = options[0];
			return (
				<DataGrid
				pagination
				paginationMode="server"
				rowCount={data.total}
				onPageChange={setPageNum}
				autoHeight={true}
				rows={data.entries}
				columns={columns}
				pageSize={instancesPerPage}
				onPageSizeChange={setInstancesPerPage}
				disableSelectionOnClick={true}
				onRowClick={(row) => navigate('/artworks/' + row.id, {state:{searchTerm: searchVal}})}
				onSortModelChange={sortByColumn}
				style={{width:"95%", margin:"auto"}}
				/>
			)
	  }

	const getInfo = async () => {
		const res = await Getdata(q + "&page=" + pageNum + "&maxResults=" + instancesPerPage)
		setInfo(res)
	}

	numPages = data?.totalPages;
	var mycmp = showCard ? Artcards : Artgrid;
	var pg    = showCard ? Paginate : Empty;
	
	pages = []
	for (var i = 1; i <= numPages; i++) {
		pages.push(i)
	}

	function Paginate(pages) {
		return(
			<nav>
			<ul className="pagination justify-content-center">
				{pages.map(number => (
					<li key={number} className={"page-item " + (mypg == number ? "active" : "")}>
					   <a onClick={() => paginate(number)} href="#!" className="page-link">
						   {number}
					   </a>
				   </li>
			   ))}
		   </ul>
			</nav>
		)
	}
	


	function paginate (pageNumber) {
		updateTableData(pageNumber, instancesPerPage)
		mypg = pageNumber;
		getInfo()
	}

	return (
		<MainWrapper>
			<h1 className="title-name text-center">Artwork</h1>
		{/* <SearchWrapper>
		  <SearchBar onSearch={handleSearch} />
		</SearchWrapper> */}
		<br></br>
		<center>
			<br></br>
	 		<span>Grid view </span>
	 		<label class="switch">
	 			<input
				  type="checkbox"
				  checked={showCard}
				  onChange = {e => setCard(e.target.checked)}
				/>
			<span class="slider"></span>
			</label>
			<span>Card view </span>
		</center>
		<center>
			<br/>
			<div style={{ maxWidth:"80%", lineHeight:1.6, margin:'auto'}}>
				<br/>
				<SearchWrapper>
		  <SearchBar onSearch={handleSearch} />
		</SearchWrapper>
			<br/>
			<FilterSelect
			filterTitles={artworkFilterTitles}
			modelFilters={artworkFilters}
			onClick={onCheckboxClick}
			/>
			</div>
		</center>
	
			{mycmp(data?.["query results"], searchVal)}
			{pg(pages)}


			<br/>
			<p className="text-center">Total Number of Artworks: {data?.total}</p>
		</MainWrapper>
	);
}
