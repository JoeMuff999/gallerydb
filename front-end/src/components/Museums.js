import React, {useEffect, useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Getdata from "./Getdata"
import Museumcards from "./Museumcards"
import styled from 'styled-components';
import { useNavigate } from "react-router";
import { makeStyles } from "@mui/styles";
import { DataGrid } from '@mui/x-data-grid';
import SearchBar from "./reuseable/SearchBar";
import FilterSelect from "./reuseable/FilterSelect";
import Empty from "./Empty"
import highlightText from "./highlightText"

const useStyles = makeStyles({
	root: {
	  '& .MuiDataGrid-columnHeaderTitle': {
		fontWeight: 'bold',
	  },
	},
  });
  
const TableWrapper = styled.div`
	width: '100%';
	font-weight: 'bold';
	margin: 2rem 0;
  `;
  
const SearchWrapper = styled.div`
	width: 100%;
	display: flex;
	justify-content: center;`

var pages = []
var numPages = 11;
var mypg = 1;


export default function Museums() {
	const classes = useStyles();
	const navigate = useNavigate();
	const [data, setData] = useState({entries:[], total: 0});
	const [pageNum, setPageNum] = useState(1);
	const [searchVal, setSearchVal] = useState("");
	const [instancesPerPage, setInstancesPerPage] = useState(18);
	const [selectedFilterState, setSelectedFilterState] = useState({});
	const [filterParamString, setFilterParamString] = useState("");
	const [sortParamString, setSortParamString] = useState("");
	const [info, setInfo] = useState();
	const [showCard, setCard] = useState(true);
	const [q, setQuery] = useState("")


	useEffect(() => {
		updateTableData(pageNum, instancesPerPage);
	}, [pageNum, instancesPerPage, searchVal, selectedFilterState, filterParamString, sortParamString]);

	const museumQueryTitles = [
		"name", "country"
	];
	
	const museumFilterTitles = [
		"Name", "Country", "Status", "Number of Artworks", "Rating"
	];
	
	const museumFilters = [
		["A-E", "F-L", "M-P", "Q-Z"],
		["US", "Italy", "France", "UK", "Germany", "Netherlands", "Switzerland", "Russia", "Spain", "Other"],
		["Open", "Temporarily closed"],
		["1", "2", "3", "4", "5", "6+"],
		["1-1.9", "2-2.9", "3-3.9", "4-5"]
	];	
	
	const filterMapping = {
		"number_of_artworks": "artwork_count",
		"Open": "OPERATIONAL",
		"Temporarily closed": "CLOSED_TEMPORARILY",
		"Rating": "rating"
	}
	
	const mapFilterValues = (filter) => {
		if (filter in filterMapping) {
			return filterMapping[filter];
		}
		return filter;
	}	

	const handleSearch = (searchValue) => {
		console.log("searching for " + searchValue);
		setSearchVal(searchValue);
		setPageNum(0);
	};	

	const onCheckboxClick = (title, value) => {
		let newState = selectedFilterState;
	
		if (title in newState) {
			if (newState[title].includes(value)) {
				newState[title] = newState[title].filter(item => item !== value);
				if (newState[title].length === 0) {
			  		delete newState[title];
				}
			}
			else {
				newState[title] = [...newState[title], value];
		  	}
		}
		else {
		  newState[title] = [value];
		}

		let filterParams = `&filter=`;
		for (let key in newState) {
			const newKey = String(key).replaceAll(" ", "_");
			filterParams += `${mapFilterValues(String(newKey).toLowerCase())}=${newState[key].map(e => mapFilterValues(e)).join(",").toLowerCase()};`;
		}
		// get rid of last ;
		filterParams = filterParams.slice(0, -1);
	
		if (filterParams !== "&filter=") {
			setFilterParamString(filterParams.replace("+", "%2B"));
		}
		else {
		  setFilterParamString("");
		}
	
		setSelectedFilterState(newState);
	}

	const columns = [
		{ field: 'name', headerName: 'Name', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1},
		{ field: 'country', headerName: 'Country', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
		{ field: 'status', headerName: 'Status', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
		{ field: 'artwork_count', headerName: 'Number of Artworks', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
		{ field: 'rating', headerName: 'Rating', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 }
	];



	  const updateTableData = (page, pageSize) => {
		console.log("Updating table")
		const newPageNum = page + 1;
		let query =`https://api.gallerydb.me/search?model=museum${searchVal !== '' ? `&general_string=${searchVal}` : ''}`
		setQuery(query)
		query += `&page=${newPageNum}&maxResults=${pageSize}`;
	
		query += filterParamString;
		query += sortParamString;
		console.log(String(query));
	
		// remove &filter if there are none selected
		const endVal = query.length;
		if (query.slice(endVal - 7, endVal) === '&filter') {
		  query = query.slice(0, endVal - 7);
		}
	
		console.log(query);
		fetch(query)
		.then((response) => {
		  if (response.ok) return response.json();
		  throw response;
		})
		.then((res) => {
		  console.log(res);
		  res.entries = res["query results"];
		  setData(res);
		})
		.catch((err) => console.error('ERROR FETCH: ', err));
	  };	

	  const sortByColumn = (e) => {
		if (e.length <= 0) return;
	
		let sortParam = `&sort=${e[0].field}&sortReverse=${e[0].sort === 'asc' ? 'false' : 'true'}`;
		setSortParamString(sortParam);
	  }
	  
	  // link text that matches page
	  const linkText = (params) => {
		try {
			return <a href={"/museums/" + params.id}>{String(params.value)}</a>
		} catch (error) {
		  console.log(error);
		}
	  };

	function Museumgrid(currentPosts) {
		return (
			<DataGrid 
			pagination
			paginationMode = "server"
			rowCount={data.total}
			onPageChange={setPageNum}
			pageSize={instancesPerPage}
			onPageSizeChange={setInstancesPerPage}
			autoHeight={true} 
			rows={data.entries} 
			columns={columns} 
			disableSelectionOnClick={true} 
			// onRowClick={(row) => history.push({pathname: '/museums/' + row.id, state:{searchTerm: searchVal}})}
			onRowClick={(row) => navigate('/museums/' + row.id, {state:{searchTerm: searchVal}})}
			onSortModelChange={sortByColumn}
			style={{width:"95%", margin:"auto"}}
			/>
		)
	}

	const getInfo = async () => {
		const res = await Getdata(q + "&page=" + pageNum + "&maxResults=" + instancesPerPage)
		setInfo(res)
	}

	var mycmp = showCard ? Museumcards : Museumgrid;
	var pg    = showCard ? Paginate    : Empty;
	
	numPages = data?.totalPages;
	pages = []
	for (var i = 1; i <= numPages; i++) {
		pages.push(i)
	}

	function paginate (pageNumber) {
		updateTableData(pageNumber, instancesPerPage)
		mypg = pageNumber;
		getInfo()
	}

	function Paginate(pages) {
		return(
			<nav>
			<ul className="pagination justify-content-center">
				{pages.map(number => (
					<li key={number} className={"page-item " + (mypg == number ? "active" : "")}>
					   <a onClick={() => {paginate(number)}} href="#!" className="page-link">
						   {number}
					   </a>
				   </li>
			   ))}
		   </ul>
			</nav>
		)
	}
	return (
		<TableWrapper className={classes.root}>
			<h1 className="title-name text-center">Museums</h1>

			<br/>
			<br/>
			<center>
				<span>Grid view </span>
				<label class="switch">
				  <input
					  type="checkbox"
					  checked={showCard}
					  onChange = {e => setCard(e.target.checked)}

					/>
				  <span class="slider"></span>
				</label>
				<span>Card view </span>
			</center>
			<center>
				<br/>
				<div style={{ maxWidth:1350, lineHeight:1.6, margin:'auto'}}>
				<br/>
				<SearchWrapper>
					<SearchBar onSearch={handleSearch}/>
				</SearchWrapper>
				<br/>
					<FilterSelect
				filterTitles = {museumFilterTitles}
				modelFilters = {museumFilters}
				onClick={onCheckboxClick}/>
				</div>
			</center>
			{mycmp(data?.["query results"], searchVal)}
			{pg(pages)}

			<br/>
			<p className="text-center">Total Number of Museums: {data?.total}</p>
		</TableWrapper>
	);
}
