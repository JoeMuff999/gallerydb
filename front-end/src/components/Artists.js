import React, {useEffect, useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import SearchBar from "./reuseable/SearchBar";
import Pagination from "./Pagination"
import Getdata from "./Getdata"
import styled from 'styled-components';
import { useNavigate } from 'react-router';
import { DataGrid } from '@mui/x-data-grid';
import { makeStyles } from '@mui/styles';
import Artistcards from "./Artistcards"
import FilterSelect from "./reuseable/FilterSelect";
import highlightText from "./highlightText"
import Empty from "./Empty"

const useStyles = makeStyles({
	root: {
		'& .MuiDataGrid-columnHeaderTitle': {
			fontWeight: 'bold',
		},
	},
});
  

const TableWrapper = styled.div`
	width: '100%';
	font-weight: 'bold';
	margin: 2rem 0;
  `;
  
const SearchWrapper = styled.div`
	width: 100%;
	display: flex;
	justify-content: center;`  

var pages = []
var numPages = 10;
var mypg = 1;

export default function Artists () {

	const classes = useStyles();
	const navigate = useNavigate();
	const [data, setData] = useState({entries:[], total: 0});
	const [pageNum, setPageNum] = useState(1);
	const [searchVal, setSearchVal] = useState("");
	const [instancesPerPage, setInstancesPerPage] = useState(15);
	const [selectedFilterState, setSelectedFilterState] = useState({});
	const [filterParamString, setFilterParamString] = useState("");
	const [sortParamString, setSortParamString] = useState("");
	const [showCard, setCard] = useState(true);
	const [q, setQuery] = useState("")


	useEffect(() => {
		updateTableData(pageNum, instancesPerPage);
	  }, [pageNum, instancesPerPage, searchVal, selectedFilterState, filterParamString, sortParamString]);
	
	const artistFilterTitles = [
		"Name", "Birth Year", "Nationality", "Gender", "Number of Artworks"
	];
	
	const artistFilters = [
		["A-E", "F-L", "M-P", "Q-Z"],
		["1300s", "1400s", "1500s", "1600s", "1700s", "1800s", "1900s"],
		["French", "Italian", "Russian", "British", "German", "American", "Dutch", "Spanish", "Other"],
		["Male", "Female"],
		["1", "2", "3", "4", "5", "6+"]
	];
	
	const filterMapping = {
		"number_of_artworks": "artwork_count",
		"birth_year": "birthday"
	}
	
	const mapFilterValues = (filter) => {
		if (filter in filterMapping) {
			return filterMapping[filter];
		}
		return filter;
	}	

	const onCheckboxClick = (title, value) => {
		let newState = selectedFilterState;
	
		if (title in newState) {
			if (newState[title].includes(value)) {
				newState[title] = newState[title].filter(item => item !== value);
				if (newState[title].length === 0) {
					delete newState[title];
				}
		  	}
			else {
				newState[title] = [...newState[title], value];
		  	}
		}
		else {
			newState[title] = [value];
		}

		let filterParams = `&filter=`;

		for (let key in newState) {
			const newKey = String(key).replaceAll(" ", "_");
			filterParams += `${mapFilterValues(String(newKey).toLowerCase())}=${newState[key].map(e => mapFilterValues(e)).join(",").toLowerCase()};`;
		}

		// get rid of last ;
		filterParams = filterParams.slice(0, -1);
	
		if (filterParams !== "&filter=") {
			setFilterParamString(filterParams.replace("+", "%2B"));
		}
		else {
			setFilterParamString("");
		}
	
		setSelectedFilterState(newState);
	}
	
	const columns = [
		{ field: 'name', headerName: 'Name', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
		{ field: 'hometown', headerName: 'Hometown', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
		{ field: 'gender', headerName: 'Gender', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
		{ field: 'nationality', headerName: 'Nationality', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
		{ field: 'artwork_count', headerName: 'Number of Art Pieces', renderCell: (params)=> (
			highlightText(String(params.value), searchVal)
		), flex: 1 },
	];

	  
	const handleSearch = (searchValue) => {
		setSearchVal(searchValue);
		setPageNum(0);
	};
	
	const sortByColumn = (e) => {
		if (e.length <= 0) return;
	
		let sortParam = `&sort=${e[0].field}&sortReverse=${e[0].sort === 'asc' ? 'false' : 'true'}`;
		setSortParamString(sortParam);
	}

	const updateTableData = (page, pageSize) => {
		console.log("Updating table")
		const newPageNum = page + 1;
		let query =`https://api.gallerydb.me/search?model=artist${searchVal !== '' ? `&general_string=${searchVal}` : ''}`
		setQuery(query)
		query += `&page=${newPageNum}&maxResults=${pageSize}`;
	
		query += filterParamString;
		query += sortParamString;
		console.log(String(query));
	
		// remove &filter if there are none selected
		const endVal = query.length;
		if (query.slice(endVal - 7, endVal) === '&filter') {
			query = query.slice(0, endVal - 7);
		}
	
		console.log(query);
		fetch(query)
			.then((response) => {
				if (response.ok)
					return response.json();
		  		throw response;
			})
			.then((res) => {
		  		console.log(res);
		  		res.entries = res["query results"];
		  		setData(res);
			})
			.catch((err) => console.error('ERROR FETCH: ', err));
	};
	
	// link text that matches page
	const linkText = (params) => {
		try {
			return <a href={"/artists/" + params.id}>{String(params.value)}</a>
		}
		catch (error) {
		  console.log(error);
		}
	};	
	// console.log(data.entries)	
	// for (var item )
	// TODO deal w me
	function Artistgrid(currentPosts) {
		return (
			<DataGrid
				pagination
				paginationMode="server"
				rowCount={data.total}
				onPageChange={setPageNum}
				autoHeight={true}
				rows={data.entries}
				columns={columns}
				pageSize={instancesPerPage}
				onPageSizeChange={setInstancesPerPage}
				disableSelectionOnClick={true}
				onRowClick={(row) => navigate('/artists/' + row.id, {state:{searchTerm: searchVal}})}
				onSortModelChange={sortByColumn}
				style={{width:"95%", margin:"auto"}}
			/>
		)
	}

	const getInfo = async () => {
		const res = await Getdata(q + "&page=" + pageNum + "&maxResults=" + instancesPerPage)
		setInfo(res)
	}

	var mycmp = showCard ? Artistcards : Artistgrid;
	var pg    = showCard ? Paginate    : Empty;
	numPages = data?.totalPages;

	pages = []
	for (var i = 1; i <= numPages; i++) {
		pages.push(i)
	}

	function paginate (pageNumber) {
		updateTableData(pageNumber, instancesPerPage)
		mypg = pageNumber;
		getInfo()
	}

	function Paginate(pages) {
		return(
			<nav>
				<ul className="pagination justify-content-center">
					{pages.map(number => (
						<li key={number} className={"page-item " + (mypg == number ? "active" : "")}>
						   <a onClick={() => paginate(number)} href="#!" className="page-link">
							   {number}
						   </a>
					   </li>
				   ))}
			   </ul>
			</nav>
		)
	}

	return (
		<TableWrapper className={classes.root}>
			<h1 className="title-name text-center">Artists</h1>

			<br/>
			<center>
				<span>Grid view </span>
				<label class="switch">
					<input
						type="checkbox"
						checked={showCard}
						onChange = {e => setCard(e.target.checked)}
					/>
					<span class="slider"/>
				</label>
				<span>Card view </span>
			</center>

			<br/>

			<center>
				<div style={{ maxWidth:1350, lineHeight:1.6, margin:'auto'}}>
					<br/>
					<SearchWrapper>
						<SearchBar onSearch={handleSearch}/>
					</SearchWrapper>
					<br/>
					<FilterSelect
						filterTitles = {artistFilterTitles}
						modelFilters = {artistFilters}
						onClick={onCheckboxClick}
					/>
				</div>
			</center>


			{mycmp(data?.["query results"], searchVal)}
			{pg(pages)}

			<br/>
			<p className="text-center">Total Number of Artists: {data?.total}</p>
		</TableWrapper>
	);
};
