
import Artcard from "./Artcard"

export default function Artcards (currentPosts, hlstr) {
	return (
		<div className="row" style={{maxWidth:"90%", margin:"auto", padding: "10px"}}>
			{
					currentPosts?.map(entry => (
						<div className="col-4">
						{Artcard(
						entry?.name,
						entry?.image,
						entry?.artist_name,
						entry?.date_created,
						entry?.museum_name,
						entry?.media?.replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase()))),
						entry?.genre?.replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase()))),
						entry?.id,
						hlstr
						)}
						<br/>
						</div>


				))
			}	
			<br/>

		</div>
	)
}
