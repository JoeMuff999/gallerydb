import React, { useState, useEffect, useRef } from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

const SearchBar = (props) => {
  const { onSearch, defaultValue } = props;
  const [value, setValue] = useState("");

  const handleChange = (e) => {
    setValue(e.target.value)
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSearch(value);
  };

  return (
    <Box
      component='form'
      sx={{
        '& > :not(style)': { m: 1, width: '25rem' },
      }}
      noValidate
      autoComplete='off'
      onSubmit={handleSubmit}
    >
      <TextField
        id='standard-basic'
        label='Search anything! Hit "Enter" to take a look!'
        variant='standard'
        onChange={handleChange}
        defaultValue={defaultValue}
      />
    </Box>
  );
};

export default SearchBar;
