import React, {useEffect, useState} from "react";
import {
  useParams,
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from "./Navbar";
import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel'
import Getdata from "./Getdata"



function Handler () {
	const [artworks, setArtworks] = useState();
	const [artists, setArtists] = useState();
	const [museums, setMuseums] = useState();

	const getArtworks = async () => {
		const res = await Getdata("https://api.gallerydb.me/artworks")
		setArtworks(res)
	}

	const getArtists = async () => {
		const res = await Getdata("https://api.gallerydb.me/artists")
		setArtists(res)
	}

	const getMuseums = async () => {
		const res = await Getdata("https://api.gallerydb.me/museums")
		setMuseums(res)
	}

	useEffect(() => {
		getArtworks();
		getArtists();
		getMuseums();
	}, [])

	// pick random values
	const randomArtwork = artworks?.artworks?.[Math.floor(Math.random() * artworks?.artworks?.length)];
	const randomArtist = artists?.artists?.[Math.floor(Math.random() * artists?.artists?.length)];
	const randomMuseum = museums?.museums?.[Math.floor(Math.random() * museums?.museums?.length)];

	return [randomArtwork, randomArtist, randomMuseum]

}

export default function Home () {

	let explore = Handler()

	return (
		<div>
			
			<h1 className="splash-title text-center">GALLERYDB</h1>

			<br/>
			<div style={{ maxWidth:800, lineHeight:1.6, margin:'auto'}}>
			<div>
				GalleryDB hopes to help people learn more about artwork, artists, and
				museums. Our goal is to make this a one-stop shop for information about
				art, and we hope to address questions such as: What artwork is made by
				which artist? What artwork is in which museum? Where are museums
				located?
			</div>
			<br/>
			<br/>

		</div>

		<br/>
		<br/>
				
		<h2 className="subheading text-center">PICKS OF THE DAY</h2>
		<div className="text-center">
			View today's top selections for an artwork, artist, and museum.
		</div>
		<br></br>
		
		<div style={{ padding:20, lineHeight:1.6, margin:'auto' }}>
		  <div class="row">
				<div class="col">
					<div className="card">
						<div className="card-header text-center"> 
							<h4><b><Link to='/artwork'>Artwork</Link> </b></h4>
						</div>
						<div className="card-body">
							<img
							  className="d-block w-100"
							  src={explore?.[0]?.image}
							/>
						</div>
						<p className="text-center">
							<b><a href={"artworks/" + explore?.[0]?.id}>{explore?.[0]?.name}</a></b>
							<br></br>
							Created by {explore?.[0]?.artist_name}
						</p>
					</div>
				</div>
				<div class="col">
					<div className="card">
						<div className="card text-center">
							<div className="card-header"> 
								<h4><b><Link to='/artists'>Artists</Link> </b></h4>
							</div>
						</div>
						<div className="card-body">
							<img
							  className="d-block w-100"
							  src={explore?.[1]?.image}
							/>
						</div>
						<p className="text-center">
							<b><a href={"artists/" + explore?.[1]?.id}>{explore?.[1]?.name}</a></b>
							<br></br>
							{explore?.[1]?.nationality} Artist
						</p>
					</div>
				</div>
				<div class="col">
					<div className="card">
						<div className="card text-center">
							<div className="card-header"> 
								<h4><b><Link to='/museums'>Museums</Link> </b></h4>
							</div>
							<div className="card-body">
								<img
								  className="d-block w-100"
								  src={explore?.[2]?.image}
								/>
							</div>
						<p className="text-center">
							<b><a href={"museums/" + explore?.[2]?.id}>{explore?.[2]?.name}</a></b>
							<br></br>
							Located at {explore?.[2]?.address}
						</p>
						</div>	
					</div>
				</div>
			</div>
			<br/>
			<br/>
		<h2 className="subheading text-center">PROJECT VIDEO</h2>
		   <center>
		   <iframe width="560" height="315" src="https://www.youtube.com/embed/YK0QtgXD9l0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</center>
			<br/>
			<br/>

			</div>
		</div>
	);
}
