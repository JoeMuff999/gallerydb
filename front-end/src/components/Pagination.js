import React, {useEffect, useState} from "react";
import {
  useParams,
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';


export default function Pagination ({pageNumber, numPages, paginate} ) {
	var pages = []
	for (var i = 1; i <= numPages; i++) {
		pages.push(i)
	}

	function cheat(number) {
		paginate(number)
		// paginate(number)
	}
	return (
		<div>
			<nav>
				<ul className="pagination justify-content-center">
					{pages.map(number => (
						<li key={number} className="page-item">
							{/* <a onClick={() => paginate(number)} href="#!" className="page-link"> */}
							<a onClick={() => paginate(number)} href="#!" className="page-link">
								{number}
							</a>
						</li>
					))}
				</ul>
			</nav>
		</div>
	);
}
