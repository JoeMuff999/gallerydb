import React from "react";

export default async function Getdata(url) {
	var response = await fetch(url);
	if (response.ok) {
		var data = await response.json()
		return data;
	}
	else {
		return "ERROR"
	}
}
