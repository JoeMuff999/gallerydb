// highlight text that matches search
export default function highlightText(text, searchVal) {
	try {
		let parts = ""
		// Split text on highlight term, include term itself into parts, ignore case
		if (searchVal !== "") {
			let parts = text.split(new RegExp(`(${searchVal})`, 'gi'));
			return <span>{parts.map(part => part.toLowerCase() === searchVal.toLowerCase() ? <mark>{part}</mark> : part)}</span>;
		}
		else {
			return <span>{text}</span>
		}
	}
	catch (error) {
		console.log(error);
	}
};
