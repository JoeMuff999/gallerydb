
export default function MakeMap(freqMap, data, filterString, isIssue) {
	var size = data.length

	for (var i = 0; i < size; i++) {
		let name = data[i][filterString]
		if (name != null) {
			if (isIssue) {
				name = name["name"]
			}
			if (freqMap.has(name)) {
				freqMap.set(name, freqMap.get(name) + 1);
			}
			else {
				freqMap.set(name, 1);
			}
		}
		
	}

	return freqMap
}
