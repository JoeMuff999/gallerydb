import Artistcard from "./Artistcard"

export default function Artistcards(currentPosts, hlstr) {
	return (
		<div className="row" style={{maxWidth:"90%", margin:"auto", padding: "10px"}}>
			{
				currentPosts?.map(entry => (
					<div className="col-4">
						{Artistcard(
						entry?.name,
						entry?.image,
						entry?.birthday,
						entry?.hometown,
						entry?.gender?.replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase()))),
						entry?.artwork_count,
						entry?.nationality,
						entry?.id,
						hlstr,
						)}
						<br/>
					</div>
				))
			}	
	
		</div>
	)
}
