import React from "react";
import Highlighter from "react-highlight-words";
import Hours from "./Hours"
import Undef from "./Undef"
var NA_STR = "Unknown"




export default function Museumcard(name = NA_STR, link = NA_STR, location = NA_STR, url = NA_STR, pieces = NA_STR, hours = NA_STR, rating, id, highlight) {
	const learn_more = "museums/" + id
	const formatted_rating = rating || NA_STR;
	var myhours = hours ? Hours : Undef;
	return(
		<div>
 		<div class="card">
			<button onClick={() => window.location.href=learn_more} className="cardButton">
			<center>
              <h4><b><Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={name}
				  /></b></h4>
			</center>
            <div class="card-body">
			 <center>
              <p>
                <img
				  alt="museum"
                  class="img-fluid"
                  height="200"
                  src={link}
                  width="200"
                />
              </p>

				</center>

				<dl class="dl-horizontal">
				  <dt> Location</dt>
				  <dd>
				  <Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={location}
				  />
				  </dd>
				  <dt> Website</dt>
					<dd> <a href={url}>{url}</a> </dd>
				  <dt> Hours </dt>
				  <dd>
					  <ul>
						{myhours(hours)}
					  </ul>
				  </dd>
				  <dt> Number of Artworks </dt>
				  <dd>
				  <Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={pieces.toString()}
				  />
				  </dd>
				  <dt> Rating (per Google Reviews) </dt>
				  <dd>
				  <Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={formatted_rating.toString()}
				  /> / 5
				  </dd>
				</dl>
			 <br/>


            </div>
			</button>
          </div>
		</div>
	);
}
