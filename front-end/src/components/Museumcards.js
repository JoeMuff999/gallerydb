import Museumcard from "./Museumcard"

export default function Museumcards(currentPosts, hlstr) {
	return (
		<div className="row" style={{maxWidth:"90%", margin:"auto", padding: "10px"}}>
			{
				currentPosts?.map(entry => (
					<div className="col-4">
						{Museumcard(
						entry?.name,
						entry?.image,
						entry?.address,
						entry?.website,
						entry?.artwork_count,
						entry?.hours,
						entry?.rating,
						entry?.id,
						hlstr
					)}
					<br/>
					</div>
				))
			}	
		</div>
	)
}
