import React, {useEffect, useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from "./Navbar";
import MakeMap from "./MakeMap"
import catherine from './imgs/catherine.png'
import john from './imgs/john.png'
import joseph from './imgs/joseph.png'
import krithika from './imgs/krithika.png'
import sumedh from './imgs/sumedh.png'

const group_members = [
  {
    name: 'Catherine Fang',
    stored: ['Catherine Fang'],
    bio: "I'm a third-year CS & Psychology student at UT Austin. I'm from Houston, TX, and I enjoy exploring the city in my free time.",
    role: 'Front-end',
	image: './imgs/catherine.png',
	tests: 7
  },
  {
    name: 'John Mackie',
    stored: ['John P Mackie'],
    bio: "I'm a third-year CS major studying at UT Austin. I'm from Dallas, Texas and spend my free time watching movies and picking up random hobbies.",
    role: 'Back-end',
	image: './imgs/john.png',
	tests: 7
  },
  {
	name: 'Joseph Muffoletto',
    stored: ['Joey Muffoletto', 'joemuff999'],
    bio: "I'm a third-year CS major in the integrated BS/MS program. Outside of school, I enjoy game development, motor racing (especially Formula 1!), and gardening.",
    role: 'Back-end',
	image: './imgs/joseph.png',
	tests: 6
  },
  {
	name: 'Krithika Ravishankar',
	stored: ['Krithika Ravishankar', 'krithravi'],
	bio: "I'm a second-year CS student at UT Austin. I'm from Dallas, TX, and I like reading and learning languages.",
	role: 'Front-end',
	image: './imgs/krithika.png',
	tests: 6
  },
  {
	name: 'Sumedh Chilakamarri',
	stored: ['Sumedh S Chilakamarri', 'Sumedh'],
	bio: "I'm a third-year CS major at UT Austin. I'm from Dallas, TX, and enjoy working out and hanging out with friends!",
	role: 'Back-end',
	image: './imgs/sumedh.png',
	tests: 7
  },
];


const tools_used = [
	{
		name: 'AWS',
		link: 'https://yt3.ggpht.com/ytc/AKedOLQP0vNXjkoKrCAYvWyOm9vEhDuBNytjbpEYi1ugD7w=s900-c-k-c0x00ffffff-no-rj',
		desc: 'A cloud platform for hosting this website',
		url: "https://aws.amazon.com"
	},
	{
		name: 'Bootstrap CSS',
		link: 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.seeklogo.net%2Fwp-content%2Fuploads%2F2016%2F06%2Fbootstrap-logo-vector-download.jpg&f=1&nofb=1',
		desc: 'A CSS framework for styling front-end elements',
		url: 'https://getbootstrap.com/'
	},
	{
		name: 'Canva',
		link: "https://www.apkmirror.com/wp-content/uploads/2019/01/5c5b88516489f.png",
		desc: "A graphic design platform for creating our logo and favicon",
		url: 'https://canva.com'
	},
	{
		name: 'Discord',
		link: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmaxcdn.icons8.com%2FShare%2Ficon%2FLogos%2Fdiscord_logo1600.png&f=1&nofb=1",
		desc: 'An instant messaging and digital distribution platform for team communication',
		url: 'https://discord.com'
	},
	{
		name: 'Gitlab',
		link: 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi1.wp.com%2Fcyberpunklibrarian.com%2Fwp-content%2Fuploads%2F2018%2F09%2Fgitlab-logo.png%3Fssl%3D1&f=1&nofb=1',
		desc: 'A DevOps platform for version control, file sharing, and issue tracking',
		url: 'https://gitlab.com'
	},
	{
		name: 'Grammarly',
		link: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.bbb.org%2FProfileImages%2F01b01dc4-f900-4de5-9d6b-6fdab0ec956c.png&f=1&nofb=1",
		desc: "A typing assistant that reviews spelling and grammar mistakes on our phase reports",
		url: 'https://grammarly.com'

	},
	{
		name: 'Namecheap',
		link: 'https://cdn.iconscout.com/icon/free/png-256/namecheap-283654.png',
		desc: 'A domain name registrar for domain name registration',
		url: 'https://namecheap.com'
	},
	{
		name: 'Postman',
		link: 'https://res.cloudinary.com/postman/image/upload/t_team_logo/v1629869194/team/2893aede23f01bfcbd2319326bc96a6ed0524eba759745ed6d73405a3a8b67a8',
		desc: 'An application used for API design and documentation',
		url: 'https://postman.com'
	},
	{
		name: 'React',
		link: 'https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Flogos-download.com%2Fwp-content%2Fuploads%2F2016%2F09%2FReact_logo_logotype_emblem.png&f=1&nofb=1',
		desc: 'A JavaScript library for building the front-end components',
		url: "https://reactjs.org/" 
	},
	{
		name: 'Jest',
		link: 'https://nx.dev/documentation/shared/jest-logo.png',
		desc: 'A JavaScript testing framework that ensures correctness of our JavaScript codebase',
		url: 'https://jestjs.io'
	},
	{
		name: 'Selenium',
		link: 'https://camo.githubusercontent.com/4b95df4d6ca7a01afc25d27159804dc5a7d0df41d8131aaf50c9f84847dfda21/68747470733a2f2f73656c656e69756d2e6465762f696d616765732f73656c656e69756d5f6c6f676f5f7371756172655f677265656e2e706e67',
		desc: 'An automation testing tool used to test GUI of website',
		url: 'https://selenium.dev'
	},
	{
		name: 'SQLAlchemy',
		link: 'https://quintagroup.com/cms/python/images/sqlalchemy-logo.png/@@images/eca35254-a2db-47a8-850b-2678f7f8bc09.png',
		desc: 'A library that facilitates the communication between Python programs and databases',
		url: 'https://sqlalchemy.org'
	},
	{
		name: 'MySQL Workbench',
		link: 'https://www.logolynx.com/images/logolynx/81/8164aa229a7f7aa5f82f2fab2d1e141b.png',
		desc: 'A visual tool to view, design, create, and maintain databases while simultaneously integrating SQL development',
		url: 'https://mysql.com'
	},
	{
		name: 'Flask',
		link: 'https://www.kindpng.com/picc/m/188-1882559_python-flask-hd-png-download.png',
		desc: 'A web framework with collections of libraries and modules to simplify code without having to worry about underlying low-level details. Used in routing when binding URL and specific endpoints to a function',
		url: 'https://palletsprojects.com/p/flask/'
	},

	{
		name: 'DB Diagram',
		link: 'https://pbs.twimg.com/profile_images/1136901164039991297/-Vt-vAYQ_400x400.png',
		desc: 'A database design tool that also helps establish relationships.',
		url: 'https://dbdiagram.io'
	},
	{
		name: 'Docker',
		link: 'https://miro.medium.com/max/336/0*iXuwZtbBvsYPPkrY.png',
		desc: 'An application build and deployment tool to package applications into containers so that code can be run in any environment.',
		url: 'https://docker.com'
	},
	{
		name: 'Gunicorn',
		link: 'https://ih1.redbubble.net/image.883315798.8466/flat,750x,075,f-pad,750x1000,f8f8f8.jpg',
		desc: 'A python HTTP server used in web app deployments that allows the flask application to run on more than one thread.',
		url: 'https://gunicorn.org'
	}
]


function Toolcard (entry) {
	return (
		<div>
			<div className="card-header text-center">
				<a href={entry["url"]}>{entry["name"]}</a>
			</div>
			<div className="card-body">
				<p>
					<img
						alt="tools-icon"
						className="img-responsive"
						height="200"
						src={entry["link"]}
						width="200"
					/>
				</p>
				<p className="card-text">
					{entry["desc"]} 
				</p>
			</div>
		</div>
	)
}

var group_images = new Map();
group_images.set("Catherine Fang", catherine)
group_images.set("John Mackie", john)
group_images.set("Joseph Muffoletto", joseph)
group_images.set("Krithika Ravishankar", krithika)
group_images.set("Sumedh Chilakamarri", sumedh)

function Personcard (entry) {
	let commits = 0;

	let stored = entry["stored"]
	let issues = issueMap.get(stored[0])


	let storedLength = entry["stored"].length
	for (let i = 0; i < storedLength; i++) {
		commits += commitMap.get(stored[i])
	}

	let image = entry["image"]

	return (
		<div>

        	<div className="card-header">
            	<h4 className="text-center"><b>{entry["name"]}</b></h4>
            </div>
            <div className="card-body">
				<p>
					<center>
						<img
							alt=""
							className="img-fluid"
							height="200"
							src={group_images.get(entry["name"])}
							width="200"
						/>
					</center>
				</p>
				<p className="card-text">
					{entry["bio"]} 
				</p>
				<p>
					<b>Major Responsibilities: </b> {entry["role"]}
				</p>
				<li><b>Number of Commits: </b> {commits} </li>
				<li><b>Issues Closed: </b> {issues}</li>
				<li><b>Number of Unit Tests: </b>{entry["tests"]}</li>
	
			</div>
			
		</div>
	)
}

// some fanciness in the absence of enums
const ISSUE = Symbol("issue")
const COMMIT = Symbol("commit")
const TEST = Symbol("test")

var commitMap = new Map();
var issueMap = new Map();

async function getResponse(freqMap, url, myStr, myBool) {

	let count = 0
	let pageNum = 1

	do {
		var response = await fetch(url + '?page=' + pageNum);
		var data = await response.json()
		MakeMap(freqMap, data, myStr, myBool)
		count += data.length;
		pageNum++;
	}
	while (data.length != 0)
	return [count, freqMap]
}

function myHandler(type) {
	var url = null;
	switch (type) {
		case ISSUE:
			url = 'https://gitlab.com/api/v4/projects/33893731/issues'
			return getResponse(issueMap, url, "closed_by", true)
		case COMMIT:
			url = 'https://gitlab.com/api/v4/projects/33893731/repository/commits'
			return getResponse(commitMap, url, "committer_name", false)
		case TEST:
			break;
		default:
			return -1
	}

}

export default function About() {


	const [issueCount, setIssueCount] = useState("-");
	const [commitCount, setCommitCount] = useState("-");
	const unitTestCount = 33; // FIXME later!

	const getIssueCount = async () => {
		const issues = await myHandler(ISSUE);
		setIssueCount(issues)
	}
	const getCommitCount = async() => {
		const commits = await myHandler(COMMIT);
		setCommitCount(commits)
	}

	useEffect(() => {
		getIssueCount();
		getCommitCount();
	}, [])

	return (
		<div>
			<div style={{ maxWidth:800, lineHeight:1.6, margin:'auto'}}>

				<h1 className="title-name text-center">About</h1>
				<h3>Description</h3>
				<p>
					GalleryDB is a website that serves as a one-stop shop for people of all
					backgrounds to learn more about artwork, artists, and museums. Our goal
					is to provide an intuitive and easily navigable product for users to
					acquire information about art, and we hope to address questions such as:
					What artwork is made by which artist? What artwork is in which museum?
					Where are museums located? We believe that this will help enhance the
					admiration for art around the world, expand our perspectives and
					understanding of each other, and further facilitate the exploration of
					various art forms, artists, and museums.
				</p>
				<h3>GitLab Repository</h3>
				<p>
					<a href="https://gitlab.com/JoeMuff999/gallerydb">https://gitlab.com/JoeMuff999/gallerydb</a>
				</p>
			</div>

			<div style={{ padding:50, lineHeight:1.6, margin:'auto' }}>
				<h2 className="subheading text-center">GROUP MEMBERS</h2>
				<br/>
				<br/>
				<div className="row d-flex justify-content-center">
					{
						group_members.map(entry => (
							<div className="col-4">
								{Personcard(entry)}
							</div>
						))
					}	
				</div>
				<br/>
				<br/>
				<div style={{ maxWidth:800, lineHeight:1.6, margin:'auto'}}>
					<h2 className="subheading text-center">PHASE LEADERS</h2>
					<br/>
					<div style={{ maxWidth:"50%", margin:'auto'}}>
						<table className="table table-bordered">
							<thead>
								<tr>
									<th scope="col">Phase #</th>
									<th scope="col">Leader</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">1</th>
									<td>Catherine Fang</td>
								</tr>
								<tr>
									<th scope="row">2</th>
									<td>John Mackie</td>
								</tr>
								<tr>
									<th scope="row">3</th>
									<td>Krithika Ravishankar</td>
								</tr>
								<tr>
									<th scope="row">4</th>
									<td>Joey Muffoletto</td>
								</tr>
							</tbody>
						</table>
					</div>

					<br/>
					<br/>

					<h2 className="subheading text-center">STATISTICS</h2>
					<br/>
					<center>
						<h3>Total commits: {commitCount[0]}</h3>
						<h3>Total issues: {issueCount[0]}</h3>
						<h3>Total unit tests: {unitTestCount} </h3>
					</center>
					<br/>
					<br/>

					<h2 className="subheading text-center">ADDITIONAL INFORMATION</h2>
					<br/>

					<p>
						<b>Our Interesting Results of Integrating Disparate Data: </b>
						Combining our data became tricky in a few ways. Many of our instances contained 
						foreign information, so they used special characters and/or formatted times (for 
						the hours of operation) and addresses differently. Canadian addresses were similar to 
						American addresses, while Ukrainian addresses were only similar to Russian addresses, 
						and many European countries followed a similar structure to one another. Additionally, 
						some artists were referenced by either their partial or full names (e.g. Picasso or Monet).
					</p>
					<br></br>

					<h3>APIs Used</h3>
					<ul>
						<li>
							<a href="https://github.com/metmuseum/openaccess">Metropolitan Museum of Art</a>
						</li>
						<li>
							<a href="https://api.artic.edu/docs/">Art Institute of Chicago</a>
						</li>
						<li>
							<a href="https://collection.cooperhewitt.org/api">Smithsonian Museums</a>
						</li>
						<li>
							<a href="https://github.com/harvardartmuseums/api-docs">Harvard Art Museums</a>
						</li>
						<li>
							<a href="https://wikipedia-api.readthedocs.io/en/latest/README.html">Wikipedia API</a>
						</li>
					</ul>
					<p>Data was scraped from APIs manually.</p>
					<br />

					<h3>Postman API link</h3>
					<p>
						<a href="https://documenter.getpostman.com/view/19739783/UVkqtFkh">https://documenter.getpostman.com/view/19739783/UVkqtFkh</a>
					</p>
					<br/>

					<h3>Tools Used</h3>
					<div className="row" >
						{
							tools_used.map(entry => (
								<div className="col-4 d-flex align-items-stretch bd-highlight example-parent">
									{Toolcard(entry)}
								</div>
							))
						}	
					</div>

				</div>
			</div>
		</div>
	);
}
