import React from "react";
import Highlighter from "react-highlight-words";
var NA_STR = "Unknown"
export default function Artcard (title = NA_STR, link = NA_STR, artist = NA_STR, year = NA_STR, location = NA_STR, medium = NA_STR, genre = NA_STR, id, highlight) {
	const learn_more = "artworks/" + id
	return (
		<div>

          <div className="card">

 			<button onClick={() => window.location.href=learn_more} className="cardButton">
			 <center>
            <div className="card-header">
              <h4><b><Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={title}
				  /></b></h4>
            </div>
			</center>
            <div className="card-body">
			  <center>
              <p>
                <img
                  alt=""
                  className="img-fluid"
                  height="200"
				  src={link}
                  width="200"
                />
              </p>
			  </center>

				<dl class="dl-horizontal">
				  <dt> Artist </dt>
				  <dd>
				  <Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={artist}
				  />
				  </dd>
				  <dt> Year</dt>
				  <dd>
				  <Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={year}
				  />
				  </dd>
				  <dt> Current Location </dt>
				  <dd>
				  <Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={location}
				  />
				  </dd>
				  <dt> Medium </dt>
				  <dd>
				  <Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={medium}
				  />
				  </dd>
				  <dt> Genre </dt>
				  <dd>
				  <Highlighter
					searchWords={highlight?.split(" ")}
					autoEscape={true}
					textToHighlight={genre}
				  />
				  </dd>

				</dl>
			  <br/>

            </div>
			</button>
          </div>
		</div>
	);
}

