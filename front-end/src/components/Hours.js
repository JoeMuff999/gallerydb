import React from "react";
import GetHours from "./GetHours"

var days = ["Monday:", "Tuesday:", "Wednesday:", "Thursday:", "Friday:", "Saturday:", "Sunday:"]

export default function Hours(input) {
	let hours = GetHours(input)	

	const ret = hours.map((entry, index) => {
		const day = days[index]
		return (
			<li>{day} {entry} </li>
		)
	})
	return ret;

}
