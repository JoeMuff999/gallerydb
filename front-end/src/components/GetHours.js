var set = new Set(["Monday:", "Tuesday:", "Wednesday:", "Thursday:", "Friday:", "Saturday:", "Sunday:"])

export default function GetHours(hours) {

	let res = []
	let mysplt = hours.split(" ")

	let size = mysplt.length

	for (var i = 0; i < size; i++) {
		let tmpstr = ""
		
		while (i < size && !set.has(mysplt[i])) {
			tmpstr += mysplt[i];
			i++;
		}
		if (tmpstr) {
			res.push(tmpstr)
		}

	}
	return res;
}
