import React from "react";
import Highlighter from "react-highlight-words";
var NA_STR = "Unknown"
export default function Artistcard(name = NA_STR, link = NA_STR, dob=NA_STR, pob = NA_STR, gender = NA_STR, pieces = NA_STR, nationality = NA_STR, id, highlight) {
	const learn_more = "artists/" + id
	return (
		<div className="card">
 			<button onClick={() => window.location.href=learn_more} className="cardButton">
				<center>
            		<div className="card-header">
              			<h4><b>
							<Highlighter
								searchWords={highlight?.split(" ")}
								autoEscape={true}
								textToHighlight={name}
				  			/>
						</b></h4>
            		</div>
				</center>
            	<div className="card-body">
					<center>
						<p>
							<img
								alt="portrait"
								className="img-fluid"
								height="200"
								src={link}
								width="200"
							/>
						</p>
					</center>


					<dl class="dl-horizontal card-text">
					  <dt> Date of Birth</dt>
					  <dd>
					  <Highlighter
						searchWords={highlight?.split(" ")}
						autoEscape={true}
						textToHighlight={dob}
					  />
					  </dd>
					  <dt> Hometown</dt>
					  <dd>
					  <Highlighter
						searchWords={highlight?.split(" ")}
						autoEscape={true}
						textToHighlight={pob}
					  />
					  </dd>
					  <dt> Nationality </dt>
					  <dd>
					  <Highlighter
						searchWords={highlight?.split(" ")}
						autoEscape={true}
						textToHighlight={nationality}
					  />
					  </dd>
					  <dt> Gender </dt>
					  <dd>
					  <Highlighter
						searchWords={highlight?.split(" ")}
						autoEscape={true}
						textToHighlight={gender}
					  />
					  </dd>
					  <dt> Number of Artworks </dt>
					  <dd>
						<Highlighter
						searchWords={highlight?.split(" ")}
						autoEscape={true}
						textToHighlight={pieces.toString()}
					  />
					  </dd>

					</dl>
					<br/>

            	</div>
			</button>
		</div>
	);
}
