import React, {useEffect, useState} from "react";
import {
  useParams,
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from "./Navbar";
import Error from "./Error";
import Getdata from "./Getdata"
import Hours from "./Hours"
import GetHours from "./GetHours"
import Undef from "./Undef"
var NA_STR = "Unknown"
var API_KEY = "AIzaSyDQjylzKAY0OYB1SZppdjFwUErlE5iU6AQ"




export default function Museumpage() {

	let {id} = useParams();

	const [info, setInfo] = useState("Retrieving data...");

	const getInfo = async () => {
		const res = await Getdata("https://api.gallerydb.me/museums/" + id)
		setInfo(res)
	}

	useEffect(() => {
		getInfo();
	}, [])

	if (info === "ERROR") {
		return <Error />
	}
	// so, this stuff is gonna change
	var name = info.name || NA_STR;
	var date = info.data || NA_STR;
	var link = info.image || NA_STR;
	var phone = info.phone || NA_STR;
	var address = info.address || NA_STR;
	var popularity = info.popularity || NA_STR;
	var number = info.number || NA_STR;
	var maps_url = info.maps_url || NA_STR;
	var myhours = info.hours ? Hours : Undef;
	var hours = info.hours || NA_STR;

	var rating = (info.rating) ? info.rating + "/5" : NA_STR;
	var website = info.website || NA_STR;
	{/*TODO check on me!! <- but this shouldn't exist ?? */}
	var artist_name = info.artist_name || [NA_STR]
	var artists = info.artists || [NA_STR]
	var art = info.art || [NA_STR];
	var art_name = info.art_name || [NA_STR];

	var museum_map = "https://www.google.com/maps/embed/v1/place?key=" + API_KEY + "&q=" + name

	return(
		<div >
			<div style={{ maxWidth:650, lineHeight:1.6, margin:'auto'}}>
				<center>
					<br/>

					<h1>{name}</h1>
						<img
						  alt=""
						  src={link}
						  width="50%"
						  height="50%"
						/>
					<br/>
				</center>
				<br/>
		
				<dl class="dl-horizontal">
				  <dt> Location </dt>
				  <dd> {address} </dd>
				  <dt> Phone </dt>
				  <dd> {phone} </dd>

				  <dt> Rating (per Tripadvisor)</dt>
				  <dd> {rating} </dd>
				  <dt> Operating Hours </dt>
				  <dd>
					  <ul>
						{myhours(hours)}
					  </ul>
				  </dd>
				  <dt> Website</dt>
				  <dd> <a href={website}> {website} </a> </dd>
				  <dt> Where is it? </dt>
				  <dd> <a href={maps_url}> {maps_url} </a> </dd>
				  <dt> Artwork</dt>
				  <dd>
					  <ul>
						{
							info?.artworks?.map((entry) =>  
								<li>	<a href={"/artworks/" + entry?.id}>{entry?.name}</a> </li>
							) 
						}

				  	  </ul>
				  </dd>
				  <dt> Artists</dt>
				  <dd>
					  <ul>
						{
							info?.artists?.map((entry) =>  
								<li>	<a href={"/artists/" + entry?.id}>{entry?.name}</a> </li>
							) 
						}

				  	  </ul>
				  </dd>


				</dl>
			<center>
				<iframe
					width="100%"

				  src={museum_map}>
				</iframe>
			</center>
			<br/>
			</div>
		</div>
	);

}

