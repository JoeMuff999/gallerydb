import React, {useEffect, useState} from "react";
import {
  useParams
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from "./Navbar";
import Error from "./Error";
import Getdata from "./Getdata"
import Carousel from 'react-bootstrap/Carousel'

var NA_STR = "Unknown"

export default function Artistpage() {
	let {id} = useParams();

	const [info, setInfo] = useState("Retrieving data...");

	const getInfo = async () => {
		const res = await Getdata("https://api.gallerydb.me/artists/" + id)
		setInfo(res)
	}

	useEffect(() => {
		getInfo();
	}, [])

	if (info === "ERROR") {
		return <Error />
	}

	var name = info.name || NA_STR;
	var link = info.image || NA_STR;
	var dob = info.birthday || NA_STR;
	var pob = info.hometown || NA_STR;
	var dod = info.deathday || NA_STR;
	var gender = info.gender || NA_STR;
	var biography = info.bio || NA_STR;
	var nationality = info.nationality || NA_STR;

	var woo = '<a href="https://google.com">hi</a>'
	return(
		<div >
			<div style={{ maxWidth:650, lineHeight:1.6, margin:'auto'}}>
				<center>
					<br/>
					<h1>{name}</h1>
						<img
						  alt=""
						  src={link}
						  width="500px"
						/>
					<br/>
				</center>
				<br/>
		
				<dl className="dl-horizontal">
				  <dt> Date of Birth </dt>
				  <dd> {dob} </dd>
				  <dt> Hometown </dt>
				  <dd> {pob} </dd>
				  <dt> Date of Death </dt>
				  <dd> {dod} </dd>
				  <dt> Gender </dt>
				  <dd> {gender} </dd>
				  <dt> Nationality </dt>
				  <dd> {nationality} </dd>
				  <dt> Biography </dt>
				  <dd> {biography} </dd>
				  <dt> Artwork </dt>
				  <dd>
					  <ul>
						{
							info?.artworks?.map((entry) =>  
								<li>	<a href={"/artworks/" + entry?.id}>{entry?.name}</a> </li>
							) 
						}

				  	  </ul>
				  </dd>
				  <dt> Museums found in </dt>
				  <dd>
					  <ul>
						{
						
							info?.museums?.map((entry) =>  
								<li>	<a href={"/museums/" + entry?.id}>{entry?.name}</a> </li>
							) 
						}

						  

				  	  </ul>
				  </dd>

				</dl>
		<Carousel>
			{
			info?.artworks?.map((entry) =>  
			  <Carousel.Item>
				<img
				  className="d-block w-100"
				  src={entry?.image}
				  alt="artslide"
				/>
				<Carousel.Caption>
				  <h3>{entry?.name}</h3>
				</Carousel.Caption>
			  </Carousel.Item>
			)
			}
		</Carousel>
			</div>
		</div>
	);

}

