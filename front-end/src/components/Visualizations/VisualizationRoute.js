import React, { useEffect } from 'react';
import styled from 'styled-components';

import ArtworkByCentury from './ArtworkByCentury';
import CountryPopularityBarChart from './CountryPopularityPieChart';
import NormalizedArtworkDistribution from './NormalizedArtworkDistribution';
// import { NationalityGenderPieChart } from "./NationalityGenderPieChart";
// import EraArtworksRadarChart from './EraArtworksRadarChart';
// import RegionMedalBarChart from "./RegionMedalBarChart";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

const TitleWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 1rem;
  margin: 1rem 0;
  background-color: lightgray;
  font-weight: bold;
`;

const VisualWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const VisualizationRoute = () => {
  useEffect(() => {
    document.title = 'GalleryDB - Artworks';
  }, []);

  return (
    <MainWrapper>
      <TitleWrapper>
        <h1>GalleryDB Visualizations</h1>
      </TitleWrapper>
      <VisualWrapper>
        <h4> Which countries have the most popular artworks? </h4>
        <h4> Top 10 Countries vs. Number of Artworks </h4>
        <CountryPopularityBarChart />
        <br />
        <h4> How does artwork popularity correlate with population? </h4>
        <h4> Number of Artworks vs. Total Population (Thousands)  </h4>
        <NormalizedArtworkDistribution />
        <br />
        <h4> When were the most famous artworks created? </h4>
        <h4> Number of Artworks vs. Century </h4>
        <ArtworkByCentury />
        <br />
      </VisualWrapper>
    </MainWrapper>
  );
};

export default VisualizationRoute;
