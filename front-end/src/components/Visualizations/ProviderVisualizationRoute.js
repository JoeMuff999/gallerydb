import React, { useEffect } from 'react';
import styled from 'styled-components';

import ProviderVis1BarChart from './ProviderVis1BarChart';
import ProviderVis2Radar from './ProviderVis2Radar';
import ProviderVis3Plot from './ProviderVis3Plot';
// import { NationalityGenderPieChart } from "./NationalityGenderPieChart";
// import EraArtworksRadarChart from './EraArtworksRadarChart';
// import RegionMedalBarChart from "./RegionMedalBarChart";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

const TitleWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 1rem;
  margin: 1rem 0;
  background-color: lightgray;
  font-weight: bold;
`;

const VisualWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ProviderVisualizationRoute = () => {
  useEffect(() => {
    document.title = 'GalleryDB - Artworks';
  }, []);

  return (
    <MainWrapper>
      <TitleWrapper>
        <h1>Provider Visualizations</h1>

      </TitleWrapper>
		<p className="text-center">Loading might take a second!</p>
      <VisualWrapper>
        <h3> Visualizations of Kitchen Next Door API data  </h3>
        <h4> Average Cost of Meal for Food Bank in Each State (average by county) </h4>
        <ProviderVis1BarChart />
        <br />
        <h4>What diets do KitchenNextDoor meals cater to most?</h4>
        <h4>Percent of Meals That Adhere to X Diet Restrictions</h4>
        <ProviderVis2Radar />
        <br />
        <h4>Do higher food prices correspond to a higher food insecurity rate?</h4>
        <h4>Price of Meal for Food Bank Vs. Percent of Food Insecurity Rate (each dot is a county)</h4>
        <ProviderVis3Plot />
        <br />
      </VisualWrapper>
    </MainWrapper>
  );
};

export default ProviderVisualizationRoute;
