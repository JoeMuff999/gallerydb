import React, { useEffect, useState } from 'react';
import {
  Pie,
  PieChart,
  Cell,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  ComposedChart,
  Scatter,
} from 'recharts';


const ArtworkByCentury = () => {

  useEffect(() => {
    let data = [];

    const query =
      'https://api.gallerydb.me/search?model=artwork&page=1&maxResults=500';
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((res) => {
        data = res['query results'];
        // yearData is a dictionary with artwork name as key and year artwork made as value
        let yearData = {}
        for(let key in data)
        {
          let entry = data[key]
          let century = Math.floor(entry.date_created/100)*100
          if(century == 0)
            continue;
          if(!(yearData[century]))
          {
            yearData[century] = {};
          }
          if (yearData[century]['dates']) {
            yearData[century]['dates'].push(entry.date_created);
          } else {
            yearData[century]['dates'] = [];
          }
          if(yearData[century]['freq'])
          {
            yearData[century]['freq'] += 1;
          }
          else{
            yearData[century]['freq'] = 1;
          }
        }
        let newData = {
          'centuryData' : [],
          'dateData' : []
        };
        for (let cent in yearData) {
          
          newData['centuryData'].push({
            century: parseInt(cent),
            freq: yearData[cent]['freq']
          })
          for(let year in yearData[cent]['dates'])
          {
            newData['dateData'].push({
              year: year
            })

          }

        }

        newData['centuryData'].sort((a, b) => {
          return (a.century - b.century);
        });
        newData['dateData'].sort((a, b) => {
          return (a.year - b.year);
        });
        console.log(newData)


        setData(newData);
      })
      .catch((err) => console.error('ERROR FETCH: ', err));
  }, []);

  const [data, setData] = useState([]);
  const tickFormatter = (value, index) => {
    const limit = 15; // put your maximum character
    if (value.length < limit) return value;
    return `${value.toString().substring(0, limit)}...`;
  };

  return (
    <ResponsiveContainer width="100%" height={300}>
    <ComposedChart
      width={1800}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray='3 3' />
      <XAxis data = {data['centuryData']} dataKey='century'/>
      <YAxis />
      <Tooltip />
      <Legend />
      {/* <Scatter data={data['dateData']}   /> */}
      <Bar data={data['centuryData']} dataKey='freq' name="Number of Artworks" fill='#1f66a4'/>
    </ComposedChart>
    </ResponsiveContainer>
  );
};

export default ArtworkByCentury;
