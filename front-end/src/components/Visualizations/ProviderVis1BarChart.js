import React, { useEffect, useState } from 'react';
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';

const ProviderVis1BarChart = () => {
  useEffect(() => {
    let data = [];
    var stateToTotalCost = {};
    var stateToCountyNumber = {};

    const query =
      'https://api.kitchennextdoor.xyz/stats/all';
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((res) => {
        console.log( typeof res);
        data = res['query results'];

        for(var i in res){
            let curr = res[i];
            if(stateToTotalCost[curr.state]){
                stateToTotalCost[curr.state]+=parseFloat(curr.costPerMeal.substring(1));
                stateToCountyNumber[curr.state]+=1.0;
            }
            else{
                stateToTotalCost[curr.state] = parseFloat(curr.costPerMeal.substring(1));
                stateToCountyNumber[curr.state]=1.0;
            }
        }

        for(var key in stateToTotalCost){
            stateToTotalCost[key] = Number((stateToTotalCost[key]/stateToCountyNumber[key]).toFixed(2))
        }

        let newData = [];
        for (let key in stateToTotalCost) {
          newData.push({
            name: key,
            cost: stateToTotalCost[key]
          })
        }
        newData.sort((a, b) => {
          return b.cost - a.cost;
        });
        setData(newData);
        data=newData;
      })
      .catch((err) => console.error('ERROR FETCH: ', err));
  }, []);

  const [data, setData] = useState([]);
  const tickFormatter = (value, index) => {
    const limit = 20; // put your maximum character
    if (value.length < limit) return value;
    return `${value.toString().substring(0, limit)}...`;
  };

  return (
    <ResponsiveContainer width="100%" height={300}>
    <BarChart
      width={2000}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray='3 3' />
      <XAxis dataKey='name' tickFormatter={tickFormatter}/>
      <YAxis unit="$"/>
      <Tooltip />
      <Legend />
      <Bar dataKey='cost' maxbarSize={290} minPointSize={5} fill='#3399ff' />
    </BarChart>
    </ResponsiveContainer>
  );
};

export default ProviderVis1BarChart;