import React, { useEffect, useState } from 'react';
import {
  Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis,
  ResponsiveContainer,
  
} from 'recharts';

const ProviderVis2Radar = () => {
  useEffect(() => {
    let data = [];
    var recipies = 0.0;
    var dietToCount = {};

    const query =
      'https://api.kitchennextdoor.xyz/recipe/all';
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((res) => {
        data = res['query results'];

        for(var i in res){

            let curr = res[i];
            let diets = curr.diets;
            for (var j in diets){
                var d=diets[j]
                if(dietToCount[d]){
                    dietToCount[d]+=1.0;
                }
                else{
                    dietToCount[d]=1.0;
                }
            }
            recipies+=1.0;
        }

        for(var key in dietToCount){
            dietToCount[key] = Number(((dietToCount[key]/recipies) * 100).toFixed(2))
        }


        let newData = [];
        for (let key in dietToCount) {
          newData.push({
            diet: key,
            percent: dietToCount[key]
          })
        }
        //sort newData by ranking
        newData.sort((a, b) => {
          return b.percent - a.percent;
        });
        // keep only top 25
        setData(newData);
        data=newData;
      })
      .catch((err) => console.error('ERROR FETCH: ', err));
  }, []);

  const [data, setData] = useState([]);


  return (

    <ResponsiveContainer width="100%" height={600}>
        <RadarChart cx="50%" cy="50%" outerRadius="80%" data={data}>
          <PolarGrid />
          <PolarAngleAxis dataKey="diet" />
          <PolarRadiusAxis/>
          <Radar name = "Mike" dataKey="percent" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
        </RadarChart>
      </ResponsiveContainer>
  );
};

export default ProviderVis2Radar;