import React, { useEffect, useState } from 'react';
import {
  Pie,
  PieChart,
  Cell,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';

const colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']


const CountryPopularityPieChart = () => {

  useEffect(() => {
    let data = [];
    let countryData = {};

    const query =
      'https://api.gallerydb.me/search?model=artwork&page=1&maxResults=500';
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((res) => {
        data = res['query results'];
        // countryData is a dictionary with Country as key and number of artworks in this country as the value
        let countryData = {}
        for(let key in data)
        {
          let entry = data[key]
          if (countryData[entry.country]) {
            countryData[entry.country] += 1;
          } else {
            countryData[entry.country] = 1;
          }
        }
        let newData = [];
        for (let key in countryData) {
          if (key === "Wherever") continue;
          newData.push({
            name: key,
            ranking: countryData[key]
          })
        }
        // sort newData by ranking
        newData.sort((a, b) => {
          return (b.ranking - a.ranking);
        });
        // keep only top 10
        newData = newData.slice(0, 10);
        setData(newData);
      })
      .catch((err) => console.error('ERROR FETCH: ', err));
  }, []);

  const [data, setData] = useState([]);

  return (
    <ResponsiveContainer width="100%" height={300}>
    <PieChart
      width={1800}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray='3 3' />
      {/* <XAxis dataKey='name' tickFormatter={tickFormatter}/> */}
      {/* <YAxis /> */}
      <Tooltip />
      <Legend />
      <Pie data={data} dataKey='ranking' fill='#1f66a4' outerRadius={100} innerRadius={20}>
        {
          data.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={colors[index]}/>
          ))
        }

      </Pie>
    </PieChart>
    </ResponsiveContainer>
  );
};

export default CountryPopularityPieChart;
