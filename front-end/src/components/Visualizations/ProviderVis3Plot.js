import React, { useEffect, useState } from 'react';
import {
    ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer 
} from 'recharts';

const ProviderVis3Plot = () => {
  useEffect(() => {
    let data = [];

    const query =
      'https://api.kitchennextdoor.xyz/stats/all';
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((res) => {
        data = res['query results'];

        let newData = [];
        for (let i in res) {
            let curr = res[i];
          newData.push({
            costPerMeal: Number(curr.costPerMeal.substring(1)),
            foodInsecurityRate: Number(curr.foodInsecurityRate.substring(0,4))
          })
        }

        newData.sort((a, b) => {
          return  a.costPerMeal-b.costPerMeal;
        });

        setData(newData);
        data=newData;
      })
      .catch((err) => console.error('ERROR FETCH: ', err));
  }, []);

  const [data, setData] = useState([]);
  const tickFormatter = (value, index) => {
    const limit = 20; // put your maximum character
    if (value.length < limit) return value;
    return `${value.toString().substring(0, limit)}...`;
  };

  return (
    <ResponsiveContainer width="100%" height={600}>
        <ScatterChart
          width={400}
          height={400}
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid />
          <XAxis dataKey="costPerMeal" unit="$" />
          <YAxis dataKey="foodInsecurityRate" unit="%" />
          
          <Scatter name="A school" data={data} fill="green" />
        </ScatterChart>
      </ResponsiveContainer>
  );
};

export default ProviderVis3Plot;