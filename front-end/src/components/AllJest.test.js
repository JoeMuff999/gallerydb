import React from "react";
import { BrowserRouter } from "react-router-dom";
import renderer from 'react-test-renderer';
// import 'regenerator-runtime/runtime'
// import About from "./About";

import MakeMap from "./MakeMap";
import Artcard from "./Artcard";
import Artistcard from "./Artistcard";
import Museumcard from "./Museumcard";
import Navbar from "./Navbar";
import Home from "./Home";
import Artwork from "./Artwork";
import Artists from "./Artists";
import Museums from "./Museums";
import About from "./About";


test("Art Card Properly Rendered", () => {
    const component = renderer.create(
        Artcard
            ("The Last Day of Pompeii", 
            "",
            "Karl Bryullov", 
            "1833", 
            "Russian Museum, Saint Petersburg, Russia", 
            "Oil Canvas", 
            "History Painting", 
            "57727100edc2cb3880c025fb",
            "")
      );
      let tree = component.toJSON();
      expect(tree).toMatchSnapshot();
})


test("Art Card Properly Rendered with Image", () => {
    const component = renderer.create(
        Artcard
            ("The Last Day of Pompeii", 
            "https://uploads4.wikiart.org/images/karl-bryullov/the-last-day-of-pompeii-1833.jpg!Large.jpg",
            "Karl Bryullov", 
            "1833", 
            "Russian Museum, Saint Petersburg, Russia", 
            "Oil Canvas", 
            "History Painting", 
            "57727100edc2cb3880c025fb",
            "")
      );
      let tree = component.toJSON();
      expect(tree).toMatchSnapshot();
})

test("Artist Card Properly Rendered", () => {
    const component = renderer.create(
        Artistcard
            ("Piet Mondrian", 
            "", 
            "March 7, 1872", 
            "Amersfoort, Netherlands", 
            "Male", 
            "14",
            "Dutch", 
            "57726d7aedc2cb3880b479c7",
            "")
      );
      let tree = component.toJSON();
      expect(tree).toMatchSnapshot();
})

test("Artist Card Properly Rendered with Image", () => {
    const component = renderer.create(
        Artistcard
            ("Piet Mondrian", 
            "https://uploads5.wikiart.org/images/piet-mondrian.jpg!Portrait.jpg", 
            "March 7, 1872", 
            "Amersfoort, Netherlands", 
            "Male", 
            "14", 
            "Dutch", 
            "57726d7aedc2cb3880b479c7",
            "")
      );
      let tree = component.toJSON();
      expect(tree).toMatchSnapshot();
})

test("Museum Card Properly Rendered", () => {
    const component = renderer.create(
        Museumcard
            ("Philadelphia Museum of Art, Philadelphia, PA, US", 
            "", 
            "2600 Benjamin Franklin Pkwy, Philadelphia, PA 19130, USA", 
            "https://www.philamuseum.org/", 
            "4", 
            "Monday: 10:00 AM – 5:00 PM Tuesday: Closed Wednesday: Closed Thursday: 10:00 AM – 5:00 PM Friday: 10:00 AM – 8:45 PM Saturday: 10:00 AM – 5:00 PM Sunday: 10:00 AM – 5:00 PM", 
            "4.8 / 5", 
            "ChIJ_5CoRebFxokR08ApAyF2KIs",
            "")
      );
      let tree = component.toJSON();
      expect(tree).toMatchSnapshot();
})

test("Museum Card Properly Rendered with Image", () => {
    const component = renderer.create(
        Museumcard
            ("Philadelphia Museum of Art, Philadelphia, PA, US", 
            "https://maps.googleapis.com/maps/api/place/photo?maxwidth=900&photo_reference=Aap_uEBfcAvC6dsQIpTMFjgp4GXuNAmeurICGV1dQiSCr_W1G8qLo1evtQpHVaMDyIv2s-rPVCEbJz8So3J9gVSOIU1dO2qify0rFX-IrSVMInRmJCn0cNyxMbcrIbMELpIQjGz7hQu9Rft46gAiwu-KpG5LOp243HKG8v3PEneeRnBX17EH&key=AIzaSyABfC8-FbUsQ9Fgjb2ZsiUMAim2brms0FE",
            "2600 Benjamin Franklin Pkwy, Philadelphia, PA 19130, USA", 
            "https://www.philamuseum.org/", 
            "4", 
            "Monday: 10:00 AM – 5:00 PM Tuesday: Closed Wednesday: Closed Thursday: 10:00 AM – 5:00 PM Friday: 10:00 AM – 8:45 PM Saturday: 10:00 AM – 5:00 PM Sunday: 10:00 AM – 5:00 PM", 
            "4.8 / 5", 
            "ChIJ_5CoRebFxokR08ApAyF2KIs",
            "")
      );
      let tree = component.toJSON();
      expect(tree).toMatchSnapshot();
})

test("Navbar Properly Rendered", () => {
    <BrowserRouter>
      render(<Navbar/>); 
      expect(screen.getByText('Home')).toBeInTheDocument();
    </BrowserRouter>;
})

test("Splash Page Properly Rendered", () => {
    <BrowserRouter>
      render(<Home/>); 
      expect(screen.getByText('GallereyDB')).toBeInTheDocument();
    </BrowserRouter>;
})

test("Artwork Page Properly Rendered", () => {
    <BrowserRouter>
      render(<Artwork/>); 
      expect(screen.getByText('Artwork')).toBeInTheDocument();
    </BrowserRouter>;
})

test("Artists Page Properly Rendered", () => {
    <BrowserRouter>
      render(<Artists/>); 
      expect(screen.getByText('Artists')).toBeInTheDocument();
    </BrowserRouter>;
})

test("Museums Page Properly Rendered", () => {
    <BrowserRouter>
      render(<Museums/>); 
      expect(screen.getByText('Museums')).toBeInTheDocument();
    </BrowserRouter>;
})

test("About Page Properly Rendered", () => {
    <BrowserRouter>
      render(<About/>); 
      expect(screen.getByText('About')).toBeInTheDocument();
    </BrowserRouter>;
})