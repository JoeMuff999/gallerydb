import React, {useEffect, useState} from "react";
import {
  useParams,
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from "./Navbar";
import Error from "./Error";
import Getdata from "./Getdata"
var NA_STR = "Unknown"
var API_KEY = "AIzaSyDQjylzKAY0OYB1SZppdjFwUErlE5iU6AQ"

export default function Artpage() {
	let {id} = useParams();

	const [info, setInfo] = useState("Retrieving data...");

	const getInfo = async () => {
		const res = await Getdata("https://api.gallerydb.me/artworks/" + id)
		setInfo(res)
	}
	useEffect(() => {
		getInfo();
	}, [])

	if (info === "ERROR") {
		return <Error />
	}

	var title = info.name || NA_STR;
	var link = info.image || NA_STR;
	var year = info.date_created || NA_STR;
	var medium = info.media?.replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase()))) || NA_STR;
	var dimensions = (info.height && info.width) ? info.height + " cm. by " + info.width + " cm." : NA_STR;
	var description = info.desc || NA_STR;
	var movement = info.genre?.replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase()))) || NA_STR;
	var artist_name = info.artist_name || NA_STR;
	var artists = '/artists/' + info?.artist?.id || NA_STR;
	var museum_name = info.museum_name || NA_STR;
	var museums = '/museums/' + info?.museum?.id;
	var museum_map = "https://www.google.com/maps/embed/v1/place?key=" + API_KEY + "&q=" + museum_name

	return (

		<div >

			<div style={{ maxWidth:650, lineHeight:1.6, margin:'auto'}}>
				<center>
					<br/>
					<h1>{title}</h1>
						<img
						  alt=""
						  src={link}
						  width="50%"
						  height="50%"
						/>
					<br/>
				</center>
				<br/>
		
				<dl class="dl-horizontal">
				  <dt> Artist </dt>
				  <dd> <a href={artists}>{artist_name}</a> </dd>
				  <dt> Year Created </dt>
				  <dd> {year} </dd>
				  <dt> Current Location </dt>
				  <dd> <a href={museums}>{museum_name}</a> </dd>
				  <dt> Medium </dt>
				  <dd> {medium} </dd>
				  <dt> Dimensions </dt>
				  <dd> {dimensions} </dd>
				  <dt> Description </dt>
				  <dd> {description} </dd>
				  <dt> Genre </dt>
				  <dd> {movement} </dd>
					<iframe
						width="100%"

					  src={museum_map}>
					</iframe>

				</dl>
			</div>
		</div>
	);

}

