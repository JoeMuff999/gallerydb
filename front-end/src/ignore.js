import { render, screen } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import App from "./App";
import Artistpage from './components/Artistpage'

// test("renders learn react link", () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

test('renders HomeRoute without crashing', () => {
  render(
      <Router>
        <HomeRoute />
      </Router>
  );
});

test('Artworks button changes route', () => {
  render(
      <Router>
        <HomeRoute />
      </Router>
  );
  fireEvent.click(screen.getAllByText("Artworks")[0])
  expect(mockHistoryPush).toHaveBeenCalledWith('/artworks')
});

test('Artists button changes route', () => {
  render(
      <Router>
        <HomeRoute />
      </Router>
  );
  fireEvent.click(screen.getAllByText("Artists")[0])
  expect(mockHistoryPush).toHaveBeenCalledWith('/artists')
});

test('Museums button changes route', () => {
  render(
      <Router>
        <HomeRoute />
      </Router>
  );
  fireEvent.click(screen.getAllByText("Museums")[0])
  expect(mockHistoryPush).toHaveBeenCalledWith('/museums')
});

test('renders Navbar without crashing', () => {
  render(
      <Router>
        <Navbar />
      </Router>
  );
});

test('renders Footer without crashing', () => {
  render(
      <Router>
        <Footer />
      </Router>
  );
});

test('Tests AboutRoute can render', async () => {
  render(
    <Router>
      <AboutRoute />
    </Router>
  );
});

test('Loads App and checks About page for text', async () => {
  render(<App/>);
  fireEvent.click(screen.getByText('About'));
  const items = await screen.findAllByText('Jack', {exact: false});
  expect(items).toHaveLength(2);
});

test('Loads AboutRoute and checks About page for pictures', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByRole('Img', {exact: false});
  expect(items).toHaveLength(5);
});

test('Loads AboutRoute and checks About page for links', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByRole('link', {exact: false});
  expect(items).toHaveLength(5);
});

test('Loads AboutRoute and checks About page for commits', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByText('Commits: ', {exact: false});
  expect(items).toHaveLength(5);
});

test('Loads AboutRoute and checks About page for team commits', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByText('Team Stats', {exact: false});
  expect(items).toHaveLength(1);
});

test('Loads AboutRoute and checks About page for issues', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByText('Issues: ', {exact: false});
  expect(items).toHaveLength(5);
});

test('Loads AboutRoute and checks About page for no specific role', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByRole('', {exact: false});
  expect(items).toHaveLength(105);
});

test('Tests Artist route shows properly', async () => {
  render(
      <App/>
  );
  fireEvent.click(screen.getAllByText("Artists")[0])
  const items = await screen.findAllByText('Artists', {exact: false});
  expect(items).toHaveLength(3);
});

test('Tests Artist route shows properly, 2', async () => {
  render(
    <App/>
  );
  fireEvent.click(screen.getAllByText("Artists")[0]);
  const items = await screen.findAllByRole('button', {exact: false});
  expect(items).toHaveLength(2);
});

test('Tests Artwork route shows properly', async () => {
  render(
      <App/>
  );
  fireEvent.click(screen.getAllByText("Artwork")[0])
  const items = await screen.findAllByText('Artists', {exact: false});
  expect(items).toHaveLength(2);
});

test('Tests Artwork route shows properly, 2', async () => {
  render(
    <App/>
  );
  fireEvent.click(screen.getAllByText("Artwork")[0])
  const items = await screen.findAllByRole('button', {exact: false});
  expect(items).toHaveLength(2);
});

test('Tests Museums route shows properly', async () => {
  render(
      <App/>
  );
  fireEvent.click(screen.getAllByText("Museums")[0])
  const items = await screen.findAllByText('Museum', {exact: false});
  expect(items).toHaveLength(3);
});

test('Tests Museum table renders properly', async () => {
  act(() => {
    render(
      <MuseumTable/>
    );
  });
  const items = await screen.findAllByRole('columnheader', {exact: false});
  expect(items).toHaveLength(3);
});
