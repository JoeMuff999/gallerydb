import unittest
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

URL = "https://www.gallerydb.me/about"

class AboutTests(unittest.TestCase):
    def setUp(cls):
        service = Service(PATH)
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(service=service, options=chrome_options)
        cls.driver.get(URL)

    def tearDown(cls):
        cls.driver.quit()

    def testAboutTitle(self):
        element = self.driver.find_element(By.CLASS_NAME, "title-name")
        self.assertEqual(element.text, "About")

    def testTeamMembersTitle(self):
        element = self.driver.find_element(by=By.TAG_NAME, value='h2')
        self.assertEqual(element.text, "GROUP MEMBERS")
  
if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
