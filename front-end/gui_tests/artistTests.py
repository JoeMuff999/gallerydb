import unittest
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.gallerydb.me/artists"
# URL = "http://localhost:3000/artists"

class ArtistTests(unittest.TestCase):
    def setUp(cls):
        service = Service(PATH)
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(service=service, options=chrome_options)
        cls.driver.get(URL)

    def tearDown(cls):
        cls.driver.quit()

    def testArtistTitle(self):
        element = self.driver.find_element(by=By.XPATH, value='/html/body/div/div/h1')
        self.assertEqual(element.text, "Artists")

    def testArtistSearch(self):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "/html/body/div/div[2]/center[2]/div/div/form/div/div/input"))
            )
        except Exception as ex:
            print("Couldn't find artist search bar: " + str(ex))
    
    def testArtistFilter(self):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "/html/body/div/div[2]/center[2]/div/form/div[3]/label[1]/input"))
            )
        except Exception as ex:
            print("Couldn't find artist name filter checkbox label: " + str(ex))
    
    def testArtistSort(self):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "/html/body/div/div[2]/center/label/span"))
            )
        except Exception as ex:
            print("Couldn't find artist name sort button: " + str(ex))
  
if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
