import os
from sys import platform

if __name__ == "__main__":
    if platform.startswith("dar"):
        PATH = "./chromedriver"
    elif platform.startswith("linux"):
        PATH = "./chromedriver"
    else:
        print("Unsupported OS")
        exit(-1)

    os.system("python3 aboutTests.py " + PATH)
    os.system("python3 artworkTests.py " + PATH)
    os.system("python3 artistTests.py " + PATH)
    os.system("python3 museumTests.py " + PATH)
    os.system("python3 homeTests.py " + PATH)
    os.system("python3 searchTests.py " + PATH)