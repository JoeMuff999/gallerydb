import unittest
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

URL = "https://www.gallerydb.me/"

class HomeTests(unittest.TestCase):
    def setUp(cls):
        service = Service(PATH)
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(service=service, options=chrome_options)
        cls.driver.get(URL)

    def tearDown(cls):
        cls.driver.quit()

    def testPicks(self):
        element = self.driver.find_element(by=By.XPATH, value='/html/body/div/div/h2')
        self.assertEqual(element.text, "PICKS OF THE DAY")

    def testPicksDescription(self):
        element = self.driver.find_element(by=By.XPATH, value='/html/body/div/div[2]/div[2]')
        self.assertEqual(element.text, "View today's top selections for an artwork, artist, and museum.")

    def testHomeDescription(self):
        element = self.driver.find_element(by=By.XPATH, value='/html/body/div/div[1]/div/nav/ul/li[3]/a').get_attribute('href')
        self.assertEqual(element, "https://www.gallerydb.me/artwork")

    def testArtworkCard(self):
        element = self.driver.find_element(by=By.XPATH, value='/html/body/div/div/div[3]/div/div[1]/div/div[1]/h4/b/a')
        self.assertEqual(element.text, "Artwork")

    def testArtistCard(self):
        element = self.driver.find_element(by=By.XPATH, value='/html/body/div/div/div[3]/div/div[2]/div/div[1]/div/h4/b/a')
        self.assertEqual(element.text, "Artists")

    def testMuseumCard(self):
        element = self.driver.find_element(by=By.XPATH, value='/html/body/div/div/div[3]/div/div[3]/div/div/div[1]/h4/b/a')
        self.assertEqual(element.text, "Museums")
  
if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])