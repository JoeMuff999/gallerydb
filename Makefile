.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

ESLINT        := npx eslint
PRETTIER      := npx prettier

HTML_TARGETS = **/*.html
JS_TARGETS =  **/*.js

# # docker-build:
# #     docker build -t gallerydbapp:latest .

# # docker-run:
# #     docker run --rm -it -v $(PWD):/front-end -w gallerydbapp:latest 



# execute eslint
# eslint:
# 	$(ESLINT) $(JS_TARGETS)

# auto format the code
format: 
	cd ./front-end && $(PRETTIER) -w $(HTML_TARGETS) $(JS_TARGETS)
	black ./back-end/*.py
	black ./back-end/db_scripts*.py

# check format of the code
check-format:
	cd ./front-end && $(PRETTIER) -c $(HTML_TARGETS) $(JS_TARGETS)

# check files, check their existence with make check
CFILES :=                                 \
    .gitlab-ci.yml                       

# check the existence of check files
check: $(CFILES)