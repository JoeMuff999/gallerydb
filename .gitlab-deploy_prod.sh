#!/bin/bash

#Get servers list
set -f
string=$BACKEND_DEPLOY_SERVER

echo "Deploy backend on server (prod/main) ${string}"
# ssh into server and run the deployment script
ssh ec2-user@${string} "cd gallerydb && git checkout main && git pull origin main && bash deploy.sh"