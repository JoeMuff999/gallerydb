#!/bin/bash

# for convenience
# unifies the building for both dev and prod (only difference is which branch to pull from, which is defined in the respective prod and dev .gitlab-deploy scripts)

cd back-end && bash deploy_backend.sh && cd ../front-end && bash deploy_frontend.sh