from init import db, app
from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
from models import (
    Artist,
    Artwork,
    Museum,
    artists_schema,
    artworks_schema,
    museums_schema,
    single_artist_schema,
    single_artwork_schema,
    single_museum_schema,
)
from models import *
import flask
import json
import math

from sqlalchemy.sql.type_api import VisitableCheckKWArg
from sqlalchemy import or_, cast, asc, desc
import sqlalchemy.sql.sqltypes as types
from re import search
from otherOptions import otherDict



@app.route("/")
def hello_world():
    return '<img src="https://ichef.bbci.co.uk/news/976/cpsprodpb/0347/production/_92593800_gettyimages-482923234.jpg" alt="welcome" />'


@app.route("/artists", methods=["GET"])
def get_artists():
    artists = Artist.query.all()
    output = artists_schema.dump(artists)
    return jsonify({"artists": output})


@app.route("/artists/<id>", methods=["GET"])
def get_artist_id(id):
    artist = Artist.query.get(id)
    if artist is None:
        response = flask.Response(
            json.dumps({"error": "Artist with id '" + id + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_artist_schema.jsonify(artist)


@app.route("/artworks", methods=["GET"])
def get_artworks():
    artworks = Artwork.query.all()
    output = artworks_schema.dump(artworks)
    return jsonify({"artworks": output})


@app.route("/artworks/<id>", methods=["GET"])
def get_artwork_id(id):
    artwork = Artwork.query.get(id)
    if artwork is None:
        response = flask.Response(
            json.dumps({"error": "Artwork with id '" + id + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_artwork_schema.jsonify(artwork)


@app.route("/museums", methods=["GET"])
def get_museums():
    museums = Museum.query.all()
    output = museums_schema.dump(museums)
    return jsonify({"museums": output})


@app.route("/museums/<id>", methods=["GET"])
def get_museum_id(id):
    museum = Museum.query.get(id)
    if museum is None:
        response = flask.Response(
            json.dumps({"error": "Museum with id '" + id + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_museum_schema.jsonify(museum)


@app.route("/search", methods=["GET"])
def query_handler():
    params = request.args.to_dict()
    model, schema = model_handler(params)
    queryInstances = model.query if model else None

    if "general_string" in params:
        if queryInstances:
            queryInstances = search_handler(
                queryInstances, params["general_string"], model.__table__.columns
            )
        else:
            return jsonify(full_search_handler(params["general_string"]))

    if "filter" in params:
        queryInstances = filter_handler(
            queryInstances, params["filter"].split(";"), model
        )

    if "sort" in params:
        sortReverse = (
            params["sortReverse"] == "true" if "sortReverse" in params else False
        )
        queryInstances = sort_handler(queryInstances, params["sort"], sortReverse)

    output, s, e, p = pagination_handler(params, queryInstances, schema)
    return jsonify(
        {
            "query results": output,
            "start": s,
            "end": e,
            "totalPages": p,
            "total": queryInstances.count(),
        }
    )


def model_handler(params):

    chosenModel = params["model"] if "model" in params else "all"
    if chosenModel == "artist":
        return (Artist, artists_schema)
    elif chosenModel == "artwork":
        return (Artwork, artworks_schema)
    elif chosenModel == "museum":
        return (Museum, museums_schema)
    else:
        return (None, None)


def search_handler(queryInstances, searchString, columns):
    condition = [
        cast(column, types.String).ilike(f"%{searchString}%") for column in columns
    ]
    return queryInstances.filter(or_(*condition))


def full_search_handler(searchString):
    models = [Artist, Artwork, Museum]
    schemas = [artists_schema, artworks_schema, museums_schema]
    names = ["artists", "artworks", "museums"]
    instances = {}
    for model, schema, name in zip(models, schemas, names):
        condition = [
            cast(column, types.String).ilike(f"%{searchString}%")
            for column in model.__table__.columns
        ]
        modelInstances = model.query.filter(or_(*condition))
        instances[name] = schema.dump(modelInstances)
    return instances


def filter_handler(queryInstances, filterList, model):
    for filter in filterList:
        condition = []
        attribute, values = filter.split("=")
        column = getattr(model, attribute)
        for value in values.split(","):
            condition.extend(attribute_handler(column, value))
        queryInstances = queryInstances.filter(or_(*condition))
    return queryInstances


def attribute_handler(column, value):
    attribute = column.name

    if attribute == "name":
        startChar, endChar = value.split("-")
        return [
            column.startswith(chr(c))
            for c in range(ord(startChar.upper()), ord(endChar.upper()) + 1)
        ]

    elif attribute == "birthday" or attribute == "date_created":
        return [column.startswith(value[0:2])]

    elif attribute == "popularity_ranking" or attribute == "artwork_count":
        if "-" in value:
            startVal, endVal = value.split("-")
            return [column.between(int(startVal), int(endVal))]
        elif "+" in value:
            startVal = value.split("+")[0]
            return [column >= int(startVal)]
        else:
            return [column == int(value)]
    
    elif attribute == "rating":
        if "-" in value:
            startVal, endVal = value.split("-")
            return [column.between(float(startVal), float(endVal))]
        elif "+" in value:
            startVal = value.split("+")[0]
            return [column >= float(startVal)]
        else:
            return [column == float(value)]
    
    elif value.lower() == "other":
        otherList = otherDict[attribute]
        return [column.ilike(f"%{val}%") for val in otherList]
    else:
        if attribute == "media":
            return [column.ilike(f"%{value}%")]
        return [column.ilike(value)]


def sort_handler(queryInstances, sortedAttribute, sortReverse):
    return (
        queryInstances.order_by(desc(sortedAttribute))
        if sortReverse
        else queryInstances.order_by(asc(sortedAttribute))
    )


def pagination_handler(params, queryInstances, schema):
    total = queryInstances.count()
    perPage = int(params["maxResults"]) if "maxResults" in params else 50
    totalPages = math.ceil(total / perPage)
    pageNum = int(params["page"]) if "page" in params else 1
    if pageNum < 1:
        pageNum = 1
    elif pageNum > totalPages:
        pageNum = totalPages
    pageInstances = queryInstances.paginate(pageNum, perPage, False)
    start = perPage * (pageNum - 1) + 1 if pageNum > 0 else 0
    end = start + perPage - 1
    end = total if end > total else end
    return schema.dump(pageInstances.items), start, end, totalPages


def setup_app(app):
    db.init_app(app)


setup_app(app)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
