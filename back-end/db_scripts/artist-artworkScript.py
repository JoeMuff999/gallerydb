from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
import requests
import json
from dotenv import load_dotenv
import os
import sys

sys.path.insert(0, "../")

from models import *

app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

load_dotenv()
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")
artsy_token = os.getenv("ARTSY_API_TOKEN")
places_key = os.getenv("PLACES_KEY")

db.app = app
db.init_app(app)
db.create_all()

most_viewed_paintings = "https://www.wikiart.org/en/api/2/MostViewedPaintings"

data = requests.get(most_viewed_paintings).json()
# print(data)

# access : eb3831b462da4b4f
# secret : b23ae6e62d66fe55
# https://www.wikiart.org/en/Api/2/login?accessCode=eb3831b462da4b4f&secretCode=b23ae6e62d66fe55


# start index, next should be 14 -> 532 (popularity). ask joey for more info if confusing
LOOP_CONST = 14
for i in range(LOOP_CONST):
    data = requests.get(
        most_viewed_paintings + "?paginationToken=" + data["paginationToken"]
    ).json()

artistList = []
artworkList = []
p_key = set()
artistDict = {}
dbArtists = Artist.query.all()
for dbArtist in dbArtists:
    artistDict[dbArtist.id] = dbArtist

#adjust after each run (set next popularity to the last pop of the last run)
popularity = 532

for i in range(1):
    print("step " + str(i))
    if "data" not in data:
        print("IM UPSET!")
    for artwork in data["data"]:
        artworkData = requests.get(
            "https://www.wikiart.org/en/api/2/Painting?id=" + artwork["id"]
        ).json()
        if (
            len(artworkData["galleries"]) != 0
            and artworkData["galleries"][0] != "Private Collection"
            and artworkData["galleries"][0] != "Destroyed"
        ):
            if artwork["artistId"] not in artistDict:
                artistData = requests.get(
                    "https://www.wikiart.org/en/" + artwork["artistUrl"] + "?json=2"
                ).json()

                artsyRequest = requests.get(
                    "https://api.artsy.net/api/artists/" + artwork["artistUrl"],
                    headers={"X-XAPP-Token": artsy_token},
                )
                artsy_hometown = (
                    artsyRequest.json()["hometown"]
                    if artsyRequest.status_code == 200
                    else "FILL IN"
                )
                artsy_nationality = (
                    artsyRequest.json()["nationality"]
                    if artsyRequest.status_code == 200
                    else "FILL IN"
                )
                new_Artist = Artist(
                    id=artwork["artistId"],
                    name=artwork["artistName"],
                    image=artistData["image"],
                    bio=artistData["biography"],
                    birthday=artistData["birthDayAsString"],
                    deathday=artistData["deathDayAsString"],
                    gender=artistData["gender"],
                    hometown=artsy_hometown,
                    nationality=artsy_nationality,
                )
                artistList.append(new_Artist)
                artistDict[artwork["artistId"]] = new_Artist

            new_Artwork = Artwork(
                id=artworkData["id"],
                name=artworkData["title"],
                image=artworkData["image"],
                genre=" ".join(artworkData["genres"]),
                media=" ".join(artworkData["media"]),
                desc=artworkData["description"],
                width=artworkData["sizeX"],
                height=artworkData["sizeY"],
                date_created=artworkData["completitionYear"],
                popularity_ranking=popularity,
                artist_name=artwork["artistName"],
                museum_name=artworkData["galleries"][0],
                artist=artistDict[artwork["artistId"]],
            )
            artworkList.append(new_Artwork)

            popularity += 1
            print(popularity)
    # data = requests.get(
    #     most_viewed_paintings + "?paginationToken=" + data["paginationToken"]
    # ).json()

db.session.add_all(artistList)
db.session.add_all(artworkList)
db.session.commit()
