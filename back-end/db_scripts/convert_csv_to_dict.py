import csv

f = open("population_by_country_2020.csv")
out = {}
for line in csv.DictReader(f, fieldnames=('Country','Population')):
    line.pop(None)
    if line['Population'] == 'Population':
        continue
    line['Population'] = int(line['Population'])
    out[line['Country']] = line['Population']

print(out)
