from imp import new_module
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer, inspect
import requests
import json
from dotenv import load_dotenv
import os
import sys

sys.path.insert(0, "../")
from models import *

app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

load_dotenv()
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")
places_key = os.getenv("PLACES_KEY")

db.app = app
db.init_app(app)
# db.session.no_autoflush()

museumList = []
museumDict = {}
idDict = {}
dbMuseums = Museum.query.all()
for dbMuseum in dbMuseums:
    museumDict[dbMuseum.name] = dbMuseum

count = 1
dbArtworks = Artwork.query.all()
dbArtists = Artist.query.all()
for i in range(0, len(dbArtworks)):
    dbArtwork = dbArtworks[i]
    if dbArtwork is None:
        continue
    if i == len(dbArtworks):
        break
    # print("CURR MUSEUM KEYS " + str(museumDict.keys()))
    if dbArtwork.museum_name not in museumDict:
        place_url = (
            "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="
            + dbArtwork.museum_name
            + "&inputtype=textquery&key="
            + places_key
        )
        place_id = requests.get(place_url).json()["candidates"][0]["place_id"]

        detailed_url = (
            "https://maps.googleapis.com/maps/api/place/details/json?fields=business_status%2Cformatted_address%2Cname%2Cphoto%2Cplace_id%2Curl%2Cinternational_phone_number%2Copening_hours%2Cwebsite%2Crating&place_id="
            + place_id
            + "&key="
            + places_key
        )

        museumData = requests.get(detailed_url).json()
        museumData = museumData["result"]
        if museumData["place_id"] in idDict:
            museumDict[dbArtwork.museum_name] = idDict[museumData["place_id"]]
        else:
            #city country
            #print(museumData)
            countryM = "unknown"
            cityM = "unknown"
            if "address_components" in museumData and len(museumData["address_components"] > 0):
                adrList = museumData["address_components"]
                for i in adrList:
                    if "types" in i:
                        if "county" in i["types"]:
                            country = i["long_name"]
                        if "administrative_area_level_2" in i["types"]:
                            city = i["short_name"]
                            print(city)

            photo_url = None
            if ("photos" in museumData and len(museumData["photos"]) > 0):
                photo_url = (
                "https://maps.googleapis.com/maps/api/place/photo?maxwidth=900&photo_reference="
                + museumData["photos"][0]["photo_reference"]
                + "&key="
                + places_key)
            
           
            phoneNum = (
                museumData["international_phone_number"]
                if "international_phone_number" in museumData
                else None
            )
            hoursList = (
                " ".join(museumData["opening_hours"]["weekday_text"])
                if "opening_hours" in museumData
                else None
            )
            stat = (
                museumData["business_status"] if "business_status" in museumData else None
            )
            ratingInfo = museumData["rating"] if "rating" in museumData else None
            site = museumData["website"] if "website" in museumData else None
            addr = (
                museumData["formatted_address"]
                if "formatted_address" in museumData
                else None
            )
            maps = museumData["url"] if "url" in museumData else None
            llat = 0
            llong = 0
            if "geometry" in museumData and "location" in museumData["geometry"] and "lat" in museumData["geometry"]["location"]:
                llat = museumData["geometry"]["location"]["lat"]
            if "geometry" in museumData and "location" in museumData["geometry"] and "long" in museumData["geometry"]["location"]:
                llong = museumData["geometry"]["location"]["lat"]

            new_Museum = Museum(
                id=museumData["place_id"],
                name=dbArtwork.museum_name,
                lat = llat,
                lng = llong,
                city = cityM,
                country = countryM,
                image=photo_url,
                status=stat,
                address=addr,
                phone=phoneNum,
                rating=ratingInfo,
                website=site,
                hours=hoursList,
                maps_url=maps,
                popularity_ranking=count,
            )
            museumList.append(new_Museum)
            museumDict[dbArtwork.museum_name] = new_Museum
            idDict[museumData["place_id"]] = new_Museum
            # print("note: " + str(museumData["place_id"]))
            # print("note name: " + dbArtwork.museum_name)
            # print()
            # print(new_Museum.id)
            # print(new_Museum.name)
            # print(new_Museum.lat)
            # print(new_Museum.lng)
            # print(new_Museum.city)
            # print(new_Museum.country)
            #print(new_Museum.image)
            # print(new_Museum.id)
            # print(new_Museum.id)
            count += 1

    
    museumObject = museumDict[dbArtwork.museum_name]
    # db.session.add(museumObject)
    # db.session.commit()

    if dbArtwork.museum == None:
        dbArtwork.museum = museumObject
    # current_states = [str(inspect(obj)) for obj in dbArtwork.artist.museums]
    # print("current list " + str(dbArtwork.artist.museums))
    # print("current states " + str(current_states))

    # print("to add: " + str(museumObject))
    # print("state = " + str(inspect(museumObject).persistent))
    if museumObject not in dbArtwork.artist.museums:
        dbArtwork.artist.museums.append(museumObject)

    print(count)
    #print(len(dbArtworks))
    # print("....")


db.session.add_all(museumList)
db.session.commit()

