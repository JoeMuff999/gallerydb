from django.shortcuts import redirect
from init import db, app
from flask import jsonify
from flask import Flask
from dotenv import load_dotenv
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
from models import (
    Artist,
    Artwork,
    Museum,
    artists_schema,
    artworks_schema,
    museums_schema,
    single_artist_schema,
    single_artwork_schema,
    single_museum_schema,
)
from models import *
#import flask
import json
import os
import sys

from models import *

app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

load_dotenv()
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")

db.app = app
db.init_app(app)

#add artwork count to artists as well as ranking
def updateFunc1():

    artists = Artist.query.all()
    print()
    print()
    print(type(artists[0]))
    print(artists[0].name)

    nameToArtCount = dict()
    for i in artists:
        nameToArtCount[i.name] = 0

    artworks = Artwork.query.all()
    for art in artworks:
        if art.artist_name in nameToArtCount:
            nameToArtCount[art.artist_name] = nameToArtCount[art.artist_name]+1

    sortedNames = [(k, v) for k, v in nameToArtCount.items()]
    sortedNames.sort(key = lambda x: -x[1])
    namesPos = dict()
    for i in range(len(sortedNames)):
        namesPos[sortedNames[i][0]] = i+1
    
    for i in artists:
        artcount = nameToArtCount[i.name]
        i.artwork_count = artcount
        i.popularity_ranking = namesPos[i.name]

    print(artists[0].id)
    print(artists[0].name)
    print(artists[0].bio)
    print(artists[0].gender)
    print(artists[0].artwork_count)
    print(artists[0].popularity_ranking)
    

def updatArtistFillIn():
    artists = Artist.query.all()
    for i in artists:
        if(i.hometown == "FILL IN"):
            i.hometown = ""
        if(i.nationality == "FILL IN"):
            i.nationality = ""


def makeTableAgain():
    # artist_to_museum = db.Table(
    #     "artist_to_museum",
    #     db.Column("artist_id", db.String(sizeOfString), db.ForeignKey("artist.id")),
    #     db.Column("museum_id", db.String(sizeOfString), db.ForeignKey("museum.id")),
    # )
    db.create_all()
#updateFunc1()
#updatArtistFillIn()
#makeTableAgain()



def findBioForArtists():
    specialHolder = {"Michelangelo" : "Michelangelo di Lodovico Buonarroti Simoni (Italian: [mikeˈlandʒelo di lodoˈviːko ˌbwɔnarˈrɔːti siˈmoːni]; 6 March 1475 – 18 February 1564), known simply as Michelangelo (English: /ˌmaɪkəlˈændʒəloʊ, ˌmɪk-/[1]), was an Italian sculptor, painter, architect and poet of the High Renaissance. Born in the Republic of Florence, his work had a major influence on the development of Western art, particularly in relation to the Renaissance notions of humanism and naturalism. He is often considered a contender for the title of the archetypal Renaissance man, along with his rival and elder contemporary, Leonardo da Vinci. Given the sheer volume of surviving correspondence, sketches, and reminiscences, Michelangelo is one of the best-documented artists of the 16th century and several scholars have described Michelangelo as the most accomplished artist of his era.",
    "Ivan Kramskoy" : "Kramskoi came from an impoverished petit-bourgeois family. From 1857 to 1863 he studied at the St. Petersburg Academy of Arts; he reacted against academic art and was an initiator of the 'Revolt of the Fourteen' which ended with the expulsion from the Academy of a group of its graduates, who organized the Artel of Artists ('Артель художников'). Influenced by the ideas of the Russian revolutionary democrats, Kramskoi asserted the high public duty of the artist, principles of realism, and the moral substance and nationality of art. He became one of the main founders and ideologists of the Company of Itinerant Art Exhibitions (or Peredvizhniki). In 1863–1868 he taught at the drawing school of a society for the promotion of applied arts. He created a gallery of portraits of important Russian writers, scientists, artists and public figures (Lev Nikolaevich Tolstoy, 1873, Ivan Shishkin, 1873, Pavel Mikhailovich Tretyakov, 1876, Mikhail Saltykov-Shchedrin, 1879, Sergei Botkin, 1880) in which expressive simplicity of composition and clarity of depiction emphasize profound psychological elements of character. Kramskoi's democratic ideals found their brightest expression in his portraits of peasants, which portrayed a wealth of character-details in representatives of the common people.",
    "Georges Seurat" : "Georges Pierre Seurat was a French post-Impressionist artist. He devised the painting techniques known as chromoluminarism and pointillism and used conté crayon for drawings on paper with a rough surface. Seurat's artistic personality combined qualities that are usually thought of as opposed and incompatible: on the one hand, his extreme and delicate sensibility, on the other, a passion for logical abstraction and an almost mathematical precision of mind. His large-scale work A Sunday Afternoon on the Island of La Grande Jatte (1884–1886) altered the direction of modern art by initiating Neo-Impressionism, and is one of the icons of late 19th-century painting.",
    "Jan van Eyck": "Jan van Eyck was a painter active in Bruges who was one of the early innovators of what became known as Early Netherlandish painting, and one of the most significant representatives of Early Northern Renaissance art. According to Vasari and other art historians including Ernst Gombrich, he invented oil painting, though most now regard that as an over-simplification. Van Eyck painted both secular and religious subject matter, including altarpieces, single-panel religious figures and commissioned portraits. His work includes single panels, diptychs, triptychs, and polyptych panels. He was well paid by Philip, who sought that the painter was secure financially and had artistic freedom so that he could paint 'whenever he pleased'. Van Eyck's work comes from the International Gothic style, but he soon eclipsed it, in part through a greater emphasis on naturalism and realism. He achieved a new level of virtuosity through his developments in the use of oil paint. He was highly influential, and his techniques and style were adopted and refined by the Early Netherlandish painters.",
    "Fra Angelico": "Fra Angelico (born Guido di Pietro; c. 1395[2] – February 18, 1455) was an Italian painter of the Early Renaissance, described by Vasari in his Lives of the Artists as having 'a rare and perfect talent'. He earned his reputation primarily for the series of frescoes he made for his own friary, San Marco, in Florence.",
    "Edward Burne-Jones": "Sir Edward Coley Burne-Jones, 1st Baronet, ARA (/bɜːrnˈdʒoʊnz/; 28 August 1833 – 17 June 1898) was a British artist and designer associated with the Pre-Raphaelite movement, who worked with William Morris on decorative arts as a founding partner in Morris, Marshall, Faulkner & Company. Burne-Jones was involved in the rejuvenation of the tradition of stained glass art in Britain; his works include windows in St. Philip's Cathedral, Birmingham, St Martin in the Bull Ring, Birmingham, Holy Trinity Church, Sloane Square, Chelsea, St Peter and St Paul parish church in Cromer, St Martin's Church in Brampton, Cumbria (the church designed by Philip Webb), St Michael's Church, Brighton, Trinity Church in Frome, All Saints, Jesus Lane, Cambridge, St Edmund Hall, and Christ Church, two colleges of the University of Oxford. His stained glass works also feature in St Anne's Church, Brown Edge, Staffordshire Moorlands, and St Edward the Confessor church at Cheddleton Staffordshire. Burne-Jones's early paintings show the inspiration of Dante Gabriel Rossetti, but by the 1860s Burne-Jones was discovering his own artistic 'voice'.",
    "Alphonse Mucha": "Alfons Maria Mucha (Czech: [ˈalfons ˈmuxa] (audio speaker iconlisten); 24 July 1860 – 14 July 1939), known internationally as Alphonse Mucha, was a Bohemian and Czech painter, illustrator, and graphic artist, living in Paris during the Art Nouveau period, best known for his distinctly stylized and decorative theatrical posters, particularly those of Sarah Bernhardt. He produced illustrations, advertisements, decorative panels, and designs, which became among the best-known images of the period. In the second part of his career, at the age of 50, he returned to his homeland of Bohemia region in Austria and devoted himself to painting a series of twenty monumental canvases known as The Slav Epic, depicting the history of all the Slavic peoples of the world, which he painted between 1912 and 1926. In 1928, on the 10th anniversary of the independence of Czechoslovakia, he presented the series to the Czech nation. He considered it his most important work. It is now on display in Prague.",
    "Balthus":"Balthasar Klossowski de Rola (February 29, 1908 – February 18, 2001), known as Balthus, was a Polish-French modern artist. He is known for his erotically charged images of pubescent girls, but also for the refined, dreamlike quality of his imagery. Throughout his career, Balthus rejected the usual conventions of the art world. He insisted that his paintings should be seen and not read about, and he resisted any attempts made to build a biographical profile.",
    "Robert Campin":"Robert Campin (c. 1375 – 26 April 1444), now usually identified with the Master of Flémalle (earlier the Master of the Merode Triptych, before the discovery of three other similar panels), was the first great master of Early Netherlandish painting. While the existence of a highly successful painter called Robert Campin is relatively well documented for the period, no works can be certainly identified as by him through a signature or contemporary documentation. A group of paintings, none dated, have been long attributed to him, and a further group were once attributed to an unknown 'Master of Flémalle'. It is now usually thought that both groupings are by Campin, but this has been a matter of some controversy for decades. ",
    "Arnold Böcklin":"He was born in Basel. His father, Christian Frederick Böcklin (b. 1802), was descended from an old family of Schaffhausen, and engaged in the silk trade. His mother, Ursula Lippe, was a native of the same city. Arnold studied at the Düsseldorf academy under Schirmer, and became a friend of Anselm Feuerbach. He is associated with the Düsseldorf school of painting. Schirmer, who recognized in him a student of exceptional promise, sent him to Antwerp and Brussels, where he copied the works of Flemish and Dutch masters. Böcklin then went to Paris, worked at the Louvre, and painted several landscapes.",
    "Eric Fischl":"Eric Fischl (born March 9, 1948) is an American painter, sculptor, printmaker, draughtsman and educator. He is known for his paintings depicting American suburbia from the 1970s and 1980s."
    }

    import wikipedia
    artists = Artist.query.all()
    count=0
    bad=[]
    for i in artists:
        if(i.bio==" "):
            count+=1
            #print(i.name)
            t =wikipedia.search(i.name)
            #print(t)
            t=str(t[0])
            s=""
            try:
                s = wikipedia.summary(t, sentences= 4, auto_suggest=True, redirect=True)
                s=s.replace("== Biography ==", "")
                s=s.replace("=== Youth and education ===", "")
                s=s.replace("== Life ==", "")
                s=s.replace("\n", "")#continue here
                i.bio = s
            except:
                print("failure on artist: ",t)
                if(i.name in specialHolder):
                    i.bio = specialHolder[i.name]
                else:
                    bad+=i.name

            print(i.name, " : ", i.bio)
            print()
    print(count)
    print(bad)


def findDescForArt():


    ################################
    print()
    print()

    import requests
    import wikipedia
    artwork = Artwork.query.all()
    failures = []
    changed=0
    c=0
    for a in artwork:
            if a.desc == "":
                c+=1
    print("how many?: ", c)

    for a in reversed(artwork):
        if a.desc == "":
            try:
                query = a.name
                url = 'https://en.wikipedia.org/w/api.php'
                params = {
                            'action':'query',
                            'format':'json',
                            'list':'search',
                            'utf8':1,
                            'srsearch':query
                        }
                data = requests.get(url, params=params).json()
                n=0
                print(a.name)
                for i in data['query']['search']:
                    print(n, ": ",i['title'])
                    n+=1
                    
                select = input()
                if(select == "exit"):
                    break
                if(select == "-2"):
                    query+= " " + a.artist_name
                    params = {
                            'action':'query',
                            'format':'json',
                            'list':'search',
                            'utf8':1,
                            'srsearch':query
                        }
                    data = requests.get(url, params=params).json()
                    n=0
                    print(a.name)
                    for i in data['query']['search']:
                        print(n, ": ",i['title'])
                        n+=1
                    select = input()
                    if(select == "exit"):
                        break
                if(select != "-1"):
                    site = data['query']['search'][int(select)]
                    sum = wikipedia.page(pageid= site['pageid']).summary
                    sum = sum.replace("\n", "")
                    print(sum)
                    yesORno = input()
                    if(yesORno == "y"):
                        a.desc = sum
                        changed+=1
            except:
                failures+= a.name
    print(changed)
    print(failures)

def fixMusCityState():
    bad=[]
    museums = Museum.query.all()
    for m in museums:
        try:
            ad = m.address
            print(ad)
            spl = ad.split(",")
            count = spl[-1]
            count = count.replace(" ","")
            print("country suggest: ", count)
            inp = input()
            if(inp=="y"):
                m.country = count
            else:
                m.country = count
            
            city=""
            if(count=="USA"):
                city = spl[1].replace(" ","")
            else:
                city = spl[-2].replace(r'[^A-Za-z]', '')
            print("city suggest: ", city)
            inp2 = input()
            if(inp2=="y"):
                m.city=city
            else:
                m.city = inp2
        except:
            bad.append( "error on "+m.name)
    print(bad)

#fixMusCityState()

def addCountryToArtwork():
    artwork = Artwork.query.all()
    for a in artwork:
        adr = a.museum_name
        c = adr.split(",")[-1].replace(" ","")
        a.country = c
        print(c)


def addMuseumCount():
    artwork = Artwork.query.all()
    museums = Museum.query.all()
    count = 0
    for a in artwork:
        mName = a.museum_name
        for m in museums:
            if(m.name == mName):
                if(m.artwork_count == None):
                    m.artwork_count = 1
                else:
                    m.artwork_count = m.artwork_count + 1
                count+=1
                break
    
    for m in museums:
        print(m.artwork_count)

    print(count)

#addCountryToArtwork()
def quickFix1():
    import re
    museums = Museum.query.all()
    for m in museums:
        if(m.country == "1146Hungary"):
            m.country = "Hungary"
            print("changed hungy\n")
        if(bool(re.search("[0-9]", m.country))):
            print(m.name)
            if(m.name.split(",")[-1]==" Ukraine"):
                m.country = "Ukraine"
                print("u")
            else:
                m.country = "Russia"
                print("r")

def quickFix2():
    museums = Artist.query.all()
    for a in museums:
        if(a.name == "Caravaggio"):
            print("here")
            a.gender == "male"
            a.nationality = "Italian"

# quickFix2()


def quickFix3():
    museums = Artist.query.all()
    i=0
    p=0
    for a in museums:
        if(a.hometown == None):
            i+=1
            a.hometown = "unknown"
    for a in museums:
        if(a.nationality == ""):
            p+=1
            a.nationality = "unknown"
    print(i)
    print(p)


def quickFix4():
    art = Artwork.query.all()
    i=0
    p=0
    for a in art:
        if(a.media == ""):
            i+=1
            a.media = "unknown"
    print(i)


def quickFix4():
    bern = 4.5
    d = dict()
    d["Kunstmuseum Bern, Bern, Switzerland"] = (4.5, "OPERATIONAL")
    d["Ashmolean Museum, Oxford, UK"] = (4.7, "OPERATIONAL")
    d["Shelburne Museum, Shelburne, VT, US"] = (4.7, "CLOSED_TEMPORARILY")
    d["Steven A. Cohen Collection, Greenwich, CT, US"] = (0.0 , "PRIVATE_COLLECTION")
    d["Wurthle Gallery, Vienna, Austria"] = (4.5 , "OPERATIONAL")
    d["Stolen"] = (0.0 , "PRIVATE_COLLECTION")
    d["El Escorial, Madrid, Spain"] = (4.5 , "OPERATIONAL")
    
    art = Museum.query.all()
    for a in art:
        if(a.name in d):
            print (a.name)
            print(d[a.name][0])
            a.rating = d[a.name][0]
            a.status = d[a.name][1]

import re
#quickFix4()

def changeLinks():
    art = Artwork.query.all()
    for a in art:
        
        a.desc = a.desc.replace("[url", "<a")
        a.desc = a.desc.replace("[/url]", "</a>")
        a.desc = a.desc.replace("[i]", "<i>")
        a.desc = a.desc.replace("[/i]", "</i>")
        a.desc = a.desc.replace("]", ">")
        
        # print(a.bio)
        idx = 0
        while idx < len(a.desc):
            if a.desc[idx]=="h" and idx+4<len(a.desc):
                if a.desc[idx:idx+5]=="href=":
                    idx+=5
                    a.desc = a.desc[:idx]+'"'+a.desc[idx:]
                    idx+=1
                    while(idx<len(a.desc)):
                        if a.desc[idx] == ">":
                            a.desc = a.desc[:idx]+'"'+a.desc[idx:]
                            break
                        idx+=1
                else:
                    idx+=1
            else:
                idx+=1
        print(a.desc)

def removeLinksFromString(string):
    string = string.replace("</a>","")
    string = string.replace("<i>","")
    string = string.replace("</i>", "")
    idx = 0
    while idx < len(string):
        if string[idx] == "<":
            i = idx+1
            while i < len(string):
                if string[i]==">":
                    #print(string[idx:i+1])
                    string = string[0:idx]+string[i+1:]
                    break
                i+=1
        else:
            idx+=1
    return string

def removeBioLinks():
    art = Artwork.query.all()
    for a in art:
        a.desc = removeLinksFromString(a.desc)
    artists = Artist.query.all()
    for a in artists:
        a.bio = removeLinksFromString(a.bio)
        



def changeLinks2():
    art = Artist.query.all()
    for a in art:
        if a.name == "Friedensreich Hundertwasser":
            a.bio = a.bio.replace(" \\<a href=\"https://www.google.com/\"\\>google\\</a\\>","")
            print(a.bio)

removeBioLinks()
#changeLinks2()
#changeLinks()
db.session.commit()

#findDescForArt()
#findBioForArtists()
#artist = Artist.query.get(id)
#artworks = Artwork.query.all()


#https://api.gallerydb.me/search?model=model&general_string=search_string_here&filter=attribute=val1,val2;attribute=val1,val2

#https://api.gallerydb.me/search?model=artwork&general_string=impressionist&filter=genre=landscape