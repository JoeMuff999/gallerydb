#!/bin/bash
# kill all running docker containers (frontend and backend)
# delete all existing containers
# build the backend image
# run it and expose port 8000
docker kill $(docker ps -q) || true 
docker system prune -af
docker build -t backend . 
docker run -dp 8000:8000 backend:latest
