import requests
from unittest import main, TestCase

ENDPOINT = "https://api.gallerydb.me"
# ENDPOINT = "http://localhost:8000/"

# test class that ensures database api calls are as expected
class Tests(TestCase):

    # ---------------------
    # Artist endpoint tests
    # ---------------------

    # Test one artist from all artists and size
    def test_artists_all(self):
        artists = requests.get(f"{ENDPOINT}/artists")
        assert artists.status_code == 200
        data = artists.json()

        #test size
        assert len(data["artists"]) == 143

        #test content
        first =  data["artists"][0] 
        # assert first["bio"]== "  "
        assert first["birthday"] =="c.1478"
        assert first["deathday"] =="1510"
        assert first["gender"] =="male"
        assert first["hometown"] =="Castelfranco Veneto, Italy"
        assert first["id"] =="57726d7aedc2cb3880b479c7"
        assert first["image"]== "https://uploads0.wikiart.org/00340/images/giorgione/self-portrait-giorgione-1.jpg!Portrait.jpg"
        assert first["name"]== "Giorgione"
        assert first["nationality"]== "Italian"

        

    # Tests artist from get artist by id
    def test_artist_instance(self):
        artist = requests.get(f"{ENDPOINT}/artists/57726d82edc2cb3880b486a0")
        assert artist.status_code == 200
        data = artist.json()
        assert data["name"] == "Vincent van Gogh"
        assert data["bio"] == "Van Gogh generally considered the greatest after Rembrandt, and one of the greatest of the Post-Impressionists. The striking colour, emphatic brushwork, and contoured forms of his work powerfully influenced the current of Expressionism in modern art. Van Gogh\u2019s art became astoundingly popular after his death, especially in the late 20th century, when his work sold for record-breaking sums at auctions around the world and was featured in blockbuster touring exhibitions."
        assert data["gender"] == "male"
        assert data["nationality"] == "Dutch"
        assert data["birthday"] == "March 30, 1853"
        assert data["deathday"] == "July 29, 1890"
        assert data["hometown"] == "Zundert, Netherlands"

    # Tests expected errors are handled for incorrect artist ids
    def test_artist_instance_error(self):
        artistResponse = requests.get(f"{ENDPOINT}/artists/99993298469872645349826754")
        assert artistResponse.status_code == 404
        data = artistResponse.json()
        assert data == {"error": "Artist with id '99993298469872645349826754' not found"}

    # ---------------------
    # Artwork endpoint tests
    # ---------------------

    # Tests for expected number of artwork instances

    # test size of all artworks and instace of artowrk in all
    def test_artworks_all(self):
        artworksResponse = requests.get(f"{ENDPOINT}/artworks")
        assert artworksResponse.status_code == 200
        data = artworksResponse.json()

        #test size
        assert len(data["artworks"]) == 531

        #test unique rankings
        nums = list()
        for d in data["artworks"]:
            num = d["popularity_ranking"]
            assert not num in nums
            nums.append(num)

        #test content
        assert data["artworks"][7] == {
            "artist_name": "Giorgione",
            "country": "France",
            "date_created": "1509",
            "desc": 'This work is one of the mysteries of European painting: in spite of its undeniable quality and epochal importance, opinions are divergent concerning both its creator and its theme. It is the outstanding masterpiece of the Venetian Renaissance, the summit of Giorgione\'s creative career, so much so that according to some it may have been painted, or at least finished, by Titian rather than Giorgione.\r\n\r\nThe painting has been interpreted as an allegory of Nature, similar to Giorgione\'s Storm, which was undeniably painted by him; it was even viewed as the first example of the modern herdsman genre. Its message must be more complex than this. It is likely that the master consciously unified several themes in this painting, and the deciphering of symbols required a degree of erudition even at the time of its creation. During the eighteenth century the painting was known by the simple name of "Pastorale" and only subsequently was it given the title "F\u00eate champ\u00eatre" or "Concert champ\u00eatre", owing to its festive mood. Modern research has pointed out that the composition is in fact an allegory of poetry.\r\n\r\nThe female figures in the foreground are probably the Muses of poetry, their nakedness reveals their divine being. The standing figure pouring water from a glass jar represents the superior tragic poetry, while the seated one holding a flute is the Muse of the less prestigious comedy or pastoral poetry. The well-dressed youth who is playing a lute is the poet of exalted lyricism, while the bareheaded one is an ordinary lyricist. The painter based this differentiation on Aristotle\'s "Poetica".\r\n\r\nThe scenery is characterized by a duality. Between the elegant, slim trees on the left, we see a multi-levelled villa, while on the right, in a lush grove, we see a shepherd playing a bagpipe. Yet the effect is completely unified. The very presence of the beautiful, mature Muses provides inspiration; the harmony of scenery and figures, colours and forms proclaims the close interrelationship between man and nature, poetry and music.',
            "genre": "allegorical painting",
            "height": 110.0,
            "id": "57726e17edc2cb3880b5f298",
            "image": "https://uploads7.wikiart.org/pastoral-concert-f\u00eate-champ\u00eatre-1509(2).jpg!Large.jpg",
            "media": "oil canvas",
            "museum_name": 'Louvre, Paris, France',
            "name": "Pastoral Concert (F\u00eate champ\u00eatre)",
            "popularity_ranking": 339,
            "width": 138.0
        }

    # test content of get museum by id
    def test_artwork_instance(self):
        artworkResponse = requests.get(f"{ENDPOINT}/artworks/57726e0cedc2cb3880b58433")
        assert artworkResponse.status_code == 200
        data = artworkResponse.json()
        assert data["artist_name"] == "Giuseppe Arcimboldo"
        assert data["name"] == "Fire"
        assert data["media"] == "oil wood"
        assert data["popularity_ranking"] == 233
        assert data["width"] == 51
        assert data["height"] == 76
        assert data["date_created"] == "1566"
        assert data["museum_name"] == "Kunsthistorisches Museum, Vienna, Austria"


    # Tests expected errors are handled for incorrect artwork ids
    def test_artwork_instance_error(self):
        artworkResponse = requests.get(f"{ENDPOINT}/artworks/436475686758")
        assert artworkResponse.status_code == 404
        data = artworkResponse.json()
        assert data == {"error": "Artwork with id '436475686758' not found"}

    # ---------------------
    # Museum endpoint tests
    # ---------------------

    # test one instance of get all museums and size
    def test_museums_all(self):
        museumsResponse = requests.get(f"{ENDPOINT}/museums")
        assert museumsResponse.status_code == 200
        data = museumsResponse.json()
        #test size
        assert len(data["museums"]) == 194
        #test content
        oneMuseum = data["museums"][0]
            
        assert oneMuseum["address"]== "2600 Benjamin Franklin Pkwy, Philadelphia, PA 19130, USA"
        #assert oneMuseum["artwork_count"] null
        #assert oneMuseum["city"] == null
        #assert oneMuseum["country"] == null
        assert oneMuseum["hours"] == "Monday: 10:00 AM \u2013 5:00 PM Tuesday: Closed Wednesday: Closed Thursday: 10:00 AM \u2013 5:00 PM Friday: 10:00 AM \u2013 8:45 PM Saturday: 10:00 AM \u2013 5:00 PM Sunday: 10:00 AM \u2013 5:00 PM"
        assert oneMuseum["id"] == "ChIJ_5CoRebFxokR08ApAyF2KIs"
        assert oneMuseum["image"] == "https://maps.googleapis.com/maps/api/place/photo?maxwidth=900&photo_reference=Aap_uEBfcAvC6dsQIpTMFjgp4GXuNAmeurICGV1dQiSCr_W1G8qLo1evtQpHVaMDyIv2s-rPVCEbJz8So3J9gVSOIU1dO2qify0rFX-IrSVMInRmJCn0cNyxMbcrIbMELpIQjGz7hQu9Rft46gAiwu-KpG5LOp243HKG8v3PEneeRnBX17EH&key=AIzaSyABfC8-FbUsQ9Fgjb2ZsiUMAim2brms0FE"
        #assert oneMuseum["lat"] == null
        #assert oneMuseum["lng"] == null
        assert oneMuseum["maps_url"] == "https://maps.google.com/?cid=10027394454499475667"
        assert oneMuseum["name"] == "Philadelphia Museum of Art, Philadelphia, PA, US"
        assert oneMuseum["phone"] == "+1 215-763-8100"
        assert oneMuseum["popularity_ranking"] == 63
        assert oneMuseum["rating"] == 4.8
        assert oneMuseum["status"] == "OPERATIONAL"
        assert oneMuseum["website"] == "https://www.philamuseum.org/"

    # check data for get museum by id
    def test_museum_instance(self):
        museumResponse = requests.get(f"{ENDPOINT}/museums/ChIJD3uTd9hx5kcR1IQvGfr8dbk")
        assert museumResponse.status_code == 200
        data = museumResponse.json()
        assert data["address"] == "Rue de Rivoli, 75001 Paris, France"
        #assert data["popularity_ranking"] == 1
        assert data["hours"] == "Monday: 9:00 AM \u2013 6:00 PM Tuesday: Closed Wednesday: 9:00 AM \u2013 6:00 PM Thursday: 9:00 AM \u2013 6:00 PM Friday: 9:00 AM \u2013 6:00 PM Saturday: 9:00 AM \u2013 6:00 PM Sunday: 9:00 AM \u2013 6:00 PM"
        #assert len(data["artwork_count"]) == 1
        #assert len(data["artists"]) == 1
        assert data["name"] == "Louvre, Paris, France"
        assert data["phone"] == "+33 1 40 20 50 50"
        assert data["rating"] == 4.7
        assert data["status"] == "OPERATIONAL"
        assert data["website"] == "https://www.louvre.fr/"

    # Tests expected errors are handled for incorrect museum ids
    def test_museum_instance_error(self):
        museumResponse = requests.get(f"{ENDPOINT}/museums/1293675128763")
        assert museumResponse.status_code == 404
        data = museumResponse.json()
        assert data == {"error": "Museum with id '1293675128763' not found"}

    
    # ---------------------
    # Query endpoint tests
    # ---------------------

    # Tests general search parameter
    def test_general_search(self):
        queryResponse = requests.get(f"{ENDPOINT}/search?general_string=color")
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        assert len(data) == 3
        assert len(data["artists"]) == 28
        assert "color" in data["artists"][0]["bio"]

    # Tests model specific search
    def test_model_search(self):
        queryResponse = requests.get(
            f"{ENDPOINT}/search?model=artwork&general_string=classic"
        )
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        assert data["total"] == 21
        assert "Classic" in data["query results"][0]["desc"]

    # Tests sorting on an attribute
    def test_sort(self):
        queryResponse = requests.get(
            f"{ENDPOINT}/search?model=artist&sort=popularity_ranking"
        )
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        results = data["query results"]
        assert results[0]["popularity_ranking"] == 1
        assert results[1]["popularity_ranking"] == 2
        assert results[2]["popularity_ranking"] == 3
        assert results[3]["popularity_ranking"] == 4

    # Tests filtering on an attribute
    def test_filter(self):
        queryResponse = requests.get(
            f"{ENDPOINT}/search?model=museum&filter=status=OPERATIONAL;country=france;city=paris"
        )
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        results = data["query results"]
        assert results[0]["status"] == "OPERATIONAL"
        assert results[0]["city"] == "Paris"
        assert results[0]["country"] == "France"
        assert results[1]["status"] == "OPERATIONAL"
        assert results[1]["city"] == "Paris"
        assert results[1]["country"] == "France"

    # Tests pagination
    def test_pagination(self):
        queryResponse = requests.get(
            f"{ENDPOINT}/search?model=artwork&maxResults=5&page=3"
        )
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        assert data["total"] == 531
        assert data["totalPages"] == 107
        assert data["start"] == 11
        assert data["end"] == 15



    
if __name__ == "__main__":
    main()
