from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import ARRAY
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field
from flask_marshmallow import Marshmallow
from marshmallow import fields
from models import *
from init import db, app
from sqlalchemy.orm import backref

ma = Marshmallow(app)

sizeOfString = 255

artist_to_museum = db.Table(
    "artist_to_museum",
    db.Column("artist_id", db.String(sizeOfString), db.ForeignKey("artist.id")),
    db.Column("museum_id", db.String(sizeOfString), db.ForeignKey("museum.id")),
)


class Artist(db.Model):
    id = db.Column(db.String(sizeOfString), primary_key=True)
    name = db.Column(db.String(sizeOfString))
    image = db.Column(db.String(sizeOfString))
    bio = db.Column(db.Text())
    birthday = db.Column(db.String(sizeOfString))
    deathday = db.Column(db.String(sizeOfString))
    gender = db.Column(db.String(sizeOfString))
    hometown = db.Column(db.String(sizeOfString))
    nationality = db.Column(db.String(sizeOfString))
    artwork_count = db.Column(db.Integer)
    popularity_ranking = db.Column(db.Integer)
    artworks = db.relationship("Artwork", backref="artist")
    museums = db.relationship("Museum", secondary=artist_to_museum)


class Artwork(db.Model):
    id = db.Column(db.String(sizeOfString), primary_key=True)
    name = db.Column(db.String(sizeOfString))
    image = db.Column(db.String(sizeOfString))
    genre = db.Column(db.String(sizeOfString))
    media = db.Column(db.String(sizeOfString))
    desc = db.Column(db.Text())
    width = db.Column(db.Float)
    country = db.Column(db.String(20))
    height = db.Column(db.Float)
    date_created = db.Column(db.String(sizeOfString))
    popularity_ranking = db.Column(db.Integer)
    artist_name = db.Column(db.String(sizeOfString))
    museum_name = db.Column(db.String(sizeOfString))
    artist_id = db.Column(db.String(sizeOfString), db.ForeignKey("artist.id"))
    museum_id = db.Column(db.String(sizeOfString), db.ForeignKey("museum.id"))


class Museum(db.Model):
    id = db.Column(db.String(sizeOfString), primary_key=True)
    name = db.Column(db.String(sizeOfString))
    image = db.Column(db.String(sizeOfString*3))
    status = db.Column(db.String(sizeOfString))
    address = db.Column(db.String(sizeOfString*2))
    city = db.Column(db.String(sizeOfString))
    country = db.Column(db.String(sizeOfString))
    phone = db.Column(db.String(sizeOfString))
    rating = db.Column(db.Float)
    website = db.Column(db.String(sizeOfString*3))
    maps_url = db.Column(db.String(sizeOfString*3))
    hours = db.Column(db.String(sizeOfString))
    artwork_count = db.Column(db.Integer)
    popularity_ranking = db.Column(db.Integer)
    lat = db.Column(db.Float)
    lng = db.Column(db.Float)
    artworks = db.relationship("Artwork", backref="museum")
    artists = db.relationship("Artist", secondary=artist_to_museum)


class ArtworkSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Artwork


class ArtistSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Artist


class MuseumSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Museum


class SingleArtistSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Artist

    artworks = ma.Nested(ArtworkSchema, many=True)
    museums = ma.Nested(MuseumSchema, many=True)


class SingleArtworkSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Artwork

    artist = ma.Nested(ArtistSchema)
    museum = ma.Nested(MuseumSchema)


class SingleMuseumSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Museum

    artworks = ma.Nested(ArtworkSchema, many=True)
    artists = ma.Nested(ArtistSchema, many=True)


artists_schema = ArtistSchema(many=True)
artworks_schema = ArtworkSchema(many=True)
museums_schema = MuseumSchema(many=True)

single_artist_schema = SingleArtistSchema()
single_artwork_schema = SingleArtworkSchema()
single_museum_schema = SingleMuseumSchema()
