# GalleryDB
Canvas / Discord group number: 11am Group 3

Project Name: GalleryDB

Presentation Video: [Link](https://www.youtube.com/watch?v=YK0QtgXD9l0&feature=youtu.be)

|   Group Member        |   EID   |    GitLab ID    |
| ---------------       | ------- | --------------  |
| Catherine Fang        | cf26768 | @cathyjmf       |
| John Mackie           | jpm4554 | @johnmackie     |
| Joseph Muffoletto     | jrm7925 | @JoeMuff999     |
| Krithika Ravishankar  | kr33277 | @krithravi      |
| Sumedh Chilakamarri   | ssc2536 | @sumedh.chilak  |

## Git SHA
Phase 1: 7a3cd7d9db69f0b74eb6672e349ca58627a2bb81

Phase 2: ca61a8cae82a155878bdd0cb54b20a6ef84c3933

Phase 3: 070ccb883469f20ab50a6e4becbbf0284e115e07

Phase 4: 1be28c23ed0a6c7ca0272e68013a9ea0e9800eec

## Phase 1
Project Leader: Catherine Fang

GitLab Pipelines: https://gitlab.com/JoeMuff999/gallerydb/-/pipelines

Website: https://gallerydb.me

|   Group Member        |   Estimated Time  |   Actual Time  |
| ---------------       | ----------------- | -------------- |
| Catherine Fang        | 15                | 33             |
| John Mackie           | 15                | 15             |
| Joseph Muffoletto     | 15                | 15             |
| Krithika Ravishankar  | 15                | 15             |
| Sumedh Chilakamarri   | 15                | 15             |

## Phase 2
Project Leader: John P Mackie

GitLab Pipelines: https://gitlab.com/JoeMuff999/gallerydb/-/pipelines

Website: https://gallerydb.me

|   Group Member        |   Estimated Time  |   Actual Time  |
| ---------------       | ----------------- | -------------- |
| Catherine Fang        | 30                | 40             |
| John Mackie           | 30                | 40             |
| Joseph Muffoletto     | 30                | 40             |
| Krithika Ravishankar  | 30                | 40             |
| Sumedh Chilakamarri   | 30                | 40             |

## Phase 3
Project Leader: Krithika Ravishankar

GitLab Pipelines: https://gitlab.com/JoeMuff999/gallerydb/-/pipelines

Website: https://gallerydb.me

|   Group Member        |   Estimated Time  |   Actual Time  |
| ---------------       | ----------------- | -------------- |
| Catherine Fang        | 20                | 20             |
| John Mackie           | 20                | 20             |
| Joseph Muffoletto     | 20                | 20             |
| Krithika Ravishankar  | 20                | 20             |
| Sumedh Chilakamarri   | 20                | 20             |

## Phase 4
Project Leader: Joseph Muffoletto

GitLab Pipelines: https://gitlab.com/JoeMuff999/gallerydb/-/pipelines

Website: https://gallerydb.me

Presentation Video: [Link](https://www.youtube.com/watch?v=YK0QtgXD9l0&feature=youtu.be)

|   Group Member        |   Estimated Time  |   Actual Time  |
| ---------------       | ----------------- | -------------- |
| Catherine Fang        | 20                | 20             |
| John Mackie           | 20                | 20             |
| Joseph Muffoletto     | 20                | 20             |
| Krithika Ravishankar  | 20                | 20             |
| Sumedh Chilakamarri   | 20                | 20             |

Comments: 
- We derived our db_script files (museumScript and artist-artworkScript) and models.py from gallerygaze
- We derived our app.py file from gallerygaze and the style/formatting from Caitlin's sustainability project
- We derived the searching, sorting, and filtering UI files (Artists.js, Artwork.js, and Museums.js) from gallerygaze
