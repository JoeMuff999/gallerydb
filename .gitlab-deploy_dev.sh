#!/bin/bash

#Get servers list
set -f
string=$BACKEND_DEPLOY_SERVER

echo "Deploy backend on server (development branch) ${string}"
# ssh into server and run the deployment script
ssh ec2-user@${string} "cd gallerydb && git checkout development && git pull origin development && bash deploy.sh"